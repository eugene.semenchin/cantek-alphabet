// overlay // 

const overlay = document.querySelector('.overlay');
const body = document.querySelector('body');

const overlayActive = function () {
    overlay.classList.add('overlay--active');
    body.classList.add('none')
};

const overlayHide = function () {
    overlay.classList.remove('overlay--active');
    body.classList.remove('none')
}
// datalist //

const searchInput = document.querySelector('.search__input');
const searchDatalist = document.querySelector('.search__datalist');
const searchOption = document.querySelectorAll('.option__item .select__item');
const searchClose = document.querySelector('.close__search');
const searchLoupe = document.querySelector('.loupe__search');

if (document.getElementsByClassName('search__input').length > 0) {

  searchInput.onfocus = function () {
    // searchDatalist.style.display = 'block';
    searchDatalist.classList.toggle('active');
    searchInput.style.borderRadius = "8px 8px 0 0";
    searchClose.classList.toggle('active');
    searchLoupe.classList.toggle('hidden');
};

searchClose.addEventListener('click', () => {
  searchDatalist.classList.remove('active');
  searchClose.classList.remove('active');
  searchLoupe.classList.remove('hidden');
  searchInput.style.borderRadius = "8px 8px 8px 8px";
})


for (let option of searchDatalist.options) {
    option.onclick = function () {
    searchInput.value = option.value;
    // searchDatalist.style.display = 'none';
    searchInput.style.borderRadius = "5px";
    }
};

searchInput.oninput = function() {
    currentFocus = -1;
    var text = searchInput.value.toUpperCase();
    for (let option of searchDatalist.options) {
      if(option.value.toUpperCase().indexOf(text) > -1){
        option.style.display = "block";
        
    }else{
      option.style.display = "none";
      }
    };
}
  
var currentFocus = -1;
  searchInput.onkeydown = function(e) {
    if(e.keyCode == 40){
      currentFocus++
     addActive(searchDatalist.options);
    }
    else if(e.keyCode == 38){
      currentFocus--
     addActive(searchDatalist.options);
    }
    else if(e.keyCode == 13){
      e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (searchDatalist.options) searchDatalist.options[currentFocus].click();
          }
    }
  }
  
  function addActive(x) {
      if (!x) return false;
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      x[currentFocus].classList.add("active");
    }
    function removeActive(x) {
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("active");
      }
    }
}
// Aside reduce //
const reduceMenu = document.querySelector('.reduce__menu');
const asideMain = document.querySelector('.aside__main');
const burgerButton = document.querySelector('.burger-menu__button');
const closeMenu = document.querySelector('.close__menu');
const asideLogo = document.querySelector('.no-aside-logo');

if (document.getElementsByClassName('reduce__menu').length > 0) {
    const asideReduce = function() {
        asideMain.classList.toggle('aside__reduce');
    }
    
    const activeMenu = function () {
        asideMain.classList.toggle('aside__visible');   
    }
    
    reduceMenu.addEventListener('click', function () {
        asideReduce();
        reduceMenu.classList.toggle('reduce__rotate');
        
        // transitionHide();
    });
    
    burgerButton.addEventListener('click', function () {
        activeMenu();
        overlayActive();
    })
    
    closeMenu.addEventListener('click', function () {
        activeMenu();
        overlayHide();
    })
}


if(window.location.toString().indexOf('order-create.html')>0) {
    asideMain.style.display = 'none';
    burgerButton.style.display = 'none';
    asideLogo.style.display = 'flex';
}

if(window.location.toString().indexOf('order-request.html')>0) {
    asideMain.style.display = 'none';
    burgerButton.style.display = 'none';
    asideLogo.style.display = 'flex';
}
const notificationButtons = document.querySelectorAll('.notification__button');
const notificationsModal = document.querySelector('.notifications__modal');
const notificationsClose = document.querySelector('.notifications-modal__close');

if (document.getElementsByClassName('notification__button').length > 0) {
    const notificationModalActive = function () {
        notificationsModal.classList.toggle('active');
    }
    
    const notificationsModalClose = function () {
        notificationsModal.classList.remove('active');
    }
    
    notificationButtons.forEach(function (i) {
        i.addEventListener("click", function () {
            notificationModalActive();
            overlayActive();
        });
    });
    
    notificationsClose.addEventListener('click', function () {
        notificationsModalClose();
        overlayHide();
    })
}
const profileButtons = document.querySelectorAll('.profile__button');
const profileModal = document.querySelector('.profile__modal');
const profileClose = document.querySelector('.profile-modal__close');

if (document.getElementsByClassName('profile__button').length > 0) {
    const profileModalActive = function () {
        profileModal.classList.toggle('active');
    }
    
    const profileModalClose = function () {
        profileModal.classList.remove('active');
    }
    
    profileButtons.forEach(function (i) {
        i.addEventListener("click", function () {
            profileModalActive();
            overlayActive();
        });
    });
    
    profileClose.addEventListener('click', function () {
        profileModalClose();
        overlayHide();
    })
}
const filterButton = document.querySelector('.filter__button');
const filtersModal = document.querySelector('.filter__modal');
const filtersClose = document.querySelector('.filters-modal__close');


if (document.getElementsByClassName('filter__modal').length > 0) {
    const filterModalActive = function () {
        filtersModal.classList.toggle('active');
    }
    
    const filtersModalClose = function () {
        filtersModal.classList.remove('active');
    }
    
    filterButton.addEventListener("click", function () {
        filterModalActive();
        overlayActive();
    });
    
    filtersClose.addEventListener('click', function () {
        filtersModalClose();
        overlayHide();
    })
}
var swiper = new Swiper(".offer__slider", {
    slidesPerView: 'auto',
    freeMode: true,
    loop: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
   
    breakpoints: {
        1: {
            slidesPerView: 1,
            spaceBetween: 24,
            resistanceRatio: 0.85,
            centeredSlides: true,
        },

        426: {
            slidesPerView: 'auto',
            spaceBetween: 14,
            centeredSlides: false,
        },

        576: {
            spaceBetween: 16,
            resistanceRatio: 0.85
        },

        768: {
            spaceBetween: 18,
            resistanceRatio: 0.85
        },

        1024: {
            spaceBetween: 24,
            resistanceRatio: 0,
            autoplay: false,
        }
    },

    navigation: {
        nextEl: '.next-offer',
        prevEl: '.prev-offer',
    },
});

var swiper = new Swiper(".offer__slider--mobile", {
    slidesPerView: 'auto',
    // freeMode: true,
    allowTouchMove: false,
    loop: true,
    autoplay: {
        delay: 1500,
        // disableOnInteraction: false,
    },
   
    breakpoints: {
        320: {
            spaceBetween: 14,
            resistanceRatio: 0.85,
        },

        576: {
            spaceBetween: 16,
            resistanceRatio: 0.85
        },

        768: {
            spaceBetween: 18,
            resistanceRatio: 0.85
        },

        1024: {
            spaceBetween: 24,
            resistanceRatio: 0,
            autoplay: false,
        }
    },

    navigation: {
        nextEl: '.next-offer',
        prevEl: '.prev-offer',
    },
});

var swiper = new Swiper(".current__slider", {
    slidesPerView: 'auto',
    freeMode: true,
    loop: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },

    breakpoints: {
        320: {
            spaceBetween: 14,
            resistanceRatio: 0.85,
        },

        576: {
            spaceBetween: 16,
            resistanceRatio: 0.85
        },

        768: {
            spaceBetween: 18,
            resistanceRatio: 0.85
        },

        1024: {
            spaceBetween: 24,
            resistanceRatio: 0,
            autoplay: false,
        }
    },

    navigation: {
        nextEl: '.next-current',
        prevEl: '.prev-current',
    },
});

var swiper = new Swiper(".card__slider-1", {
    loop: true,
    spaceBetween: 10,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesProgress: true,
    breakpoints: {
        576: {
            slidesPerView: 3,
        },

        768: {
            slidesPerView: 4,
        },
    }

  });
  var swiper2 = new Swiper(".card__slider-2", {
    loop: true,
    spaceBetween: 20,
    thumbs: {
      swiper: swiper,
    },

    navigation: {
        nextEl: '.next-card',
        prevEl: '.prev-card',
    },
});


const tableCard = document.querySelectorAll('.mobile-slider-card');

tableCard.forEach((card) => {
    card.querySelector('.slider--active').addEventListener('click', () => {
       card.classList.toggle('slider'); 
    })

    card.querySelector('.close__slider').addEventListener('click', () => {
        card.classList.remove('slider'); 
    })

    var swiper = new Swiper(card.querySelector('.table-card__slider'), {
        cssMode: true,
        centeredSlides: true,
        roundLengths: true,
        navigation: {
          nextEl: card.querySelector('.table-card-next'),
          prevEl: card.querySelector('.table-card-prev'),
        },
        pagination: {
          el: card.querySelector('.swiper-pagination'),
        },
        mousewheel: true,
        keyboard: true,
    });
})
// Основные табы //

const tabContainers = document.querySelectorAll('.tab-container');
const tabsScroll = document.querySelector('.section__tabs');

tabContainers.forEach((container) => {
	const tabButtons = container.querySelectorAll('[data-tab-target]');
	const tabContents = container.querySelectorAll('.tab__content');
	const nextTabButton = container.querySelector('.next__tab');
	const prevTabButton = container.querySelector('.prev__tab');
	
	if (tabButtons.length && tabContents.length) {
		tabButtons.forEach((tab) => {
			tab.addEventListener("click", () => {
				tabButtons.forEach((tab) => {
					tab.classList.remove("active");
				});
				
				tab.classList.add("active");
		
				tabContents.forEach((tabContent) => {
					tabContent.classList.remove("active");
				});
		
				const target = document.querySelector(tab.dataset.tabTarget);
				target.classList.add("active");
			});
		});
	}

	if (nextTabButton && prevTabButton) {	
		
		const currentTab = container.querySelectorAll('.section__tab');
		const currenttTabActive = container.querySelector('.section__tab.active');
		const currentNoTab = container.querySelector('.section__tab.no-active');
		const nextTab = currentTab.nextElementSibling;
		const prevTab = currentNoTab.previousElementSibling;
		const currentContent = container.querySelectorAll('.tab__content')
		const currentNoContent = container.querySelector('.tab__content.no-active')
		const nextContent = currentContent.nextElementSibling;
		const prevContent = currentNoContent.previousElementSibling;
		var i = 0;
		var q = 0;

		nextTabButton.addEventListener('click', () => {
			currentTab[i].classList.remove("active");
			i++;

			if (i >= currentTab.length) {
			i = 0;
			}

			currentTab[i].classList.add("active");

			currentContent[q].classList.remove("active");
			q++;

			if (q >= currentContent.length) {
			q = 0;
			}

			currentContent[q].classList.add("active");
		})

		prevTabButton.addEventListener('click', () => {
			currentTab[i].classList.remove("active");
			i--;

			if (i < 0) {
			i = currentTab.length - 1;
			}
			currentTab[i].classList.add("active");

			currentContent[q].classList.remove("active");
			q--;

			if (q < 0) {
			q = currentContent.length - 1;
			}
			currentContent[q].classList.add("active");
		})
	}
})

// Табы с общим Amountм //

const maintabs = document.querySelectorAll('.main__tabs');
maintabs.forEach((maintab) => {
	const subTabButtons = maintab.querySelectorAll('.main__tab');

	subTabButtons.forEach((subTabButton) => {
	subTabButton.addEventListener('click', function (event) {

		// снова прохоSTмся по Allм кнопкам чтобы Delete класс
			subTabButtons.forEach((sub) => {
		sub.classList.remove('active');
			})

			// добавляем класс только на ту кнопку, на которуб кликнули
		this.classList.add('active');
		
		let filter = this.dataset.filter;
		let trs = document.querySelectorAll('.subcontent');

		trs.forEach(function(tr) {
				// если фильтр "All"
				if (filter === 'all') {
					// удаляем All классы
					tr.classList.remove('hide');
					tr.classList.remove('show');
				} else { // если фильтр любой кроме "All"
					// если у карточки в списке классов есть нужный класс
					if (tr.classList.contains(filter + '__subcontent')) {
						// удаляем класс если он есть
						tr.classList.remove('hide');
						tr.classList.add('show');
					} else { // если карточка не фильтруемая
						//скрываем
						tr.classList.add('hide');
						tr.classList.remove('show');
					}
				}
			});
	})
	})
})

if (document.getElementsByClassName('mob-table-cards').length > 0) {
	function Tabs() {
		var bindAll = function() {
		  var menuElements = document.querySelectorAll('[data-tab]');
		  for(var i = 0; i < menuElements.length ; i++) {
			menuElements[i].addEventListener('click', change, false);
		  }
		}
	  
		var clear = function() {
		  var menuElements = document.querySelectorAll('[data-tab]');
		  for(var i = 0; i < menuElements.length ; i++) {
			menuElements[i].classList.remove('active');
			var id = menuElements[i].getAttribute('data-tab');
			document.getElementById(id).classList.remove('active');
		  }
		}
	  
		var change = function(e) {
		  clear();
		  e.target.classList.add('active');
		  var id = e.currentTarget.getAttribute('data-tab');
		  document.getElementById(id).classList.add('active');
		}
	  
		bindAll();
	  }
	  
	var connectTabs = new Tabs();

}

// Табы оформить //

const productTab = document.querySelector('.product-basket__tab');
const listRequestTab = document.querySelector('.list-request__tab');
const makeOrder = document.querySelector('.make-order');
const makeRequest = document.querySelector('.make-request');

if (document.getElementsByClassName('product-basket__tab').length > 0) {
	listRequestTab.addEventListener('click', () => {
		makeRequest.classList.add('active');
		makeOrder.classList.add('hidden');
	})
	
	productTab.addEventListener('click', () => {
		makeRequest.classList.remove('active');
		makeOrder.classList.remove('hidden');
	})
}
const select = document.querySelectorAll('.select');
// const optionItem = document.querySelectorAll('.select__list .select__item');
// const selected = document.querySelector('.selected');
const profileSelected = document.querySelectorAll('.profile__selected');

if (document.getElementsByClassName('select').length > 0) {
    select.forEach((sel) => {

        const arrow = sel.querySelector('.arrow__button');

        sel.addEventListener('click', function() {
            this.classList.toggle('active');
            arrow.classList.toggle('active');
            
            profileSelected.forEach((profiles) => {
                profiles.classList.toggle('hidden')
            })

        });
        
        const optionItem = sel.querySelectorAll('.select__list .custom__item');
        const selected = sel.querySelector('.selected');

        console.log(optionItem)
        optionItem.forEach((element) => {
            element.addEventListener('click', function(evt) {
                console.log('Syka')
                selected.innerHTML = evt.currentTarget.textContent;
            })
        })
    })
}

if (document.getElementsByClassName('profile-select__button').length > 0) {

    const profileSelectLists = document.querySelectorAll('.profile-select__list');

    profileSelectLists.forEach((profileSelectList) => {
        const profileSelectButtons = [...profileSelectList.querySelectorAll('.profile-select__button')];
        const profileSelectionButton = profileSelectList.querySelector('.profile-selection__button')
            profileSelectButtons.forEach(profileSelectButton => {
                profileSelectButton.addEventListener('click', () => {
                    profileSelectList.querySelector('.profile-select__button.active').classList.remove('active');
                    profileSelectButton.classList.add('active');
                    profileSelectionButton.classList.add('active');
                    
            })
        })
    })
}
const resumeDialogueButton = document.querySelector('.resume__dialogue')
const dialogueChange = document.querySelector('.dialogue__change');
const dialogueButton = document.querySelectorAll('.dialogue__button');

if (document.getElementsByClassName('appeal__block').length > 0) {
    resumeDialogueButton.addEventListener('click', function() {
        dialogueChange.style.display = 'flex';
        resumeDialogueButton.style.display = 'none';
    });
    
    
    dialogueButton.forEach((button) => {
        button.addEventListener('click', function() {
            dialogueChange.style.display = 'none';
            resumeDialogueButton.style.display = 'flex';
        })
    });
}
const calendar = document.querySelector('.calendar__modal');
const calendarInput = document.querySelectorAll('.calendar__input');
// const daysTag = document.querySelector(".days");
// const currentDate = document.querySelector(".data__title");
// const prevNextIcon = document.querySelectorAll(".current-date .data__button");
const actCalendar = document.querySelector('.act-calendar__modal');
const actCalendarButton = document.querySelector('.act__button')
const calendarTable = document.querySelectorAll('.calendar__table');

if (document.getElementsByClassName('calendar__input').length > 0) {

    calendarTable.forEach((table) => {
        const daysTag = table.querySelector(".days");
        const currentDate = table.querySelector(".data__title");
        const prevNextIcon = table.querySelectorAll(".current-date .data__button");
        
        let currYear = new Date().getFullYear();
        let currMonth = new Date().getMonth();
    
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    
        const renderCalendar = () => {
            const date = new Date(currYear, currMonth, 1);
            let firstDayofMonth = date.getDay();
            let lastDateofMonth = new Date(currYear, currMonth + 1, 0).getDate();
            let lastDayofMonth = new Date(currYear, currMonth, lastDateofMonth).getDay();
            let lastDateofLastMonth = new Date(currYear, currMonth, 0).getDate();
    
            let liTag = "";
    
            for (let i = firstDayofMonth; i > 0; i--) {
                liTag += `<li class="day inactive">${lastDateofLastMonth - i + 1}</li>`;
            }
    
            for (let i = 1; i <= lastDateofMonth; i++) {
                let isToday = i === new Date().getDate() && currMonth === new Date().getMonth() && currYear === new Date().getFullYear() ? "today" : "";
                liTag += `<li class="day ${isToday}">${i}</li>`;
            }
    
            for (let i = lastDayofMonth; i < 6; i++) {
                liTag += `<li class="day inactive">${i - lastDayofMonth + 1}</li>`
            }
    
            currentDate.innerText = `${months[currMonth]} ${currYear}`;
            daysTag.innerHTML = liTag;
        };
    
        renderCalendar();
    
        prevNextIcon.forEach(icon => {
            icon.addEventListener("click", () => {
                currMonth = icon.id === "prev" ? currMonth - 1 : currMonth + 1;
    
                if (currMonth < 0 || currMonth > 11) {
                    currYear = icon.id === "prev" ? currYear - 1 : currYear + 1;
                    currMonth = currMonth < 0 ? 11 : 0;
                }
    
                renderCalendar();
            });
        });
    })

    const calendarShow = () => {
        calendar.classList.toggle('active');
    }


    calendarInput.forEach((inputcal) => {
        inputcal.addEventListener('click', () => {
            calendarShow();
            overlayActive();
        })
    })
}

if (document.getElementsByClassName('act__button').length > 0) {
    actCalendarButton.addEventListener('click', () => {
        actCalendar.classList.toggle('active');
        overlayActive();
    }) 
}
const dropdownBlocks = document.querySelectorAll('.dropdown__block');
const dropdownButton = document.querySelector('.dropdown__button');
const dropdownContent = document.querySelector('.dropdown__content');
const dropupButton = document.querySelector('.personal-manager__button');
const asideHeight = document.querySelector('.aside-height');
const backgroundLines = document.querySelectorAll('.background-line');

if (document.getElementsByClassName('dropdown__button').length > 0) {
    dropdownBlocks.forEach((dropdownBlock) => {
        var dropdownButton = dropdownBlock.querySelector('.dropdown__button');
        var dropdownContent = dropdownBlock.querySelector('.dropdown__content');
        var dropdownArrow = dropdownBlock.querySelector('.arrow__button');
       
        dropdownButton.addEventListener('click', function () {
            dropdownContent.classList.toggle('active');
           
            if (dropdownButton.classList.contains('properties-show')) {
                dropdownButton.innerHTML = (dropdownButton.innerHTML === 'Collapse properties') ? dropdownButton.innerHTML = 'Show properties' : dropdownButton.innerHTML = 'Collapse properties';
            }

            if (dropdownArrow.classList.contains('arrow-contains')) {
                dropdownArrow.classList.toggle('active');
            } 
        })
    })
}

if (document.getElementsByClassName('aside__links').length > 0) {
    // aside dropdown //
    // const asideMain = document.querySelector('.aside__main');
    const asidelinksButton = document.querySelector('.aside__links');

    asidelinksButton.addEventListener('click', () => {
        asidelinksButton.classList.toggle('active');
    })

        if (asideMain.classList.contains('aside__reduce')) {
            asidelinksButton.addEventListener('click', () => {
                asidelinksButton.classList.remove('active');
            })
    }
}

if (document.getElementsByClassName('custom-scroll__table').length > 0) {
    const customTable = document.querySelector(".table--more");
    const buttonTableMore = document.querySelector(".show--more");

    buttonTableMore.addEventListener('click', () => {
        customTable.classList.toggle('active');
        buttonTableMore.classList.toggle('hidden');
    })
}

if (document.getElementsByClassName('aside__main-navigation').length > 0) {
    dropupButton.addEventListener('click', () => {
        asideHeight.classList.toggle('active');
    })

}

if (document.getElementsByClassName('background-line').length > 0) {
    backgroundLines.forEach((lines) => {
        var backgroundButton = lines.querySelector('.background__button');
       
        backgroundButton.addEventListener('click', function () {
            lines.classList.toggle('active');
        })
    })
}
// текст //
if (document.getElementsByClassName('categories__modal').length > 0) {

    const category1 = ` 
        <a href="sub-product-catalog.html" class="category__wrapper">
            <img class="category__image" src="img/catalog/product/item-1.png" alt="item-1">
            <h5 class="category__title">Crystal</h5>
        </a>
        <span>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="12" height="12" x="0" y="0" viewBox="0 0 480.026 480.026" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path d="m475.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312-134.368 149.28c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/><path d="m267.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312L4.114 389.293c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/></g></svg>
        </span>
    `;

    const category2 = ` 
        <a href="sub-product-catalog.html" class="category__wrapper">
            <img class="category__image" src="img/catalog/product/item-2.png" alt="item-2">
            <div>
                <h5 class="category__title">Diamond</h5>
                <p class="card__subtitle">173 products</p>
            </div>
        </a>
        <span>
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="12" height="12" x="0" y="0" viewBox="0 0 480.026 480.026" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path d="m475.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312-134.368 149.28c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/><path d="m267.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312L4.114 389.293c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/></g></svg>
        </span>
    `;

    const category3 = ` 
        <a href="sub-product-catalog.html" class="category__wrapper">
            <img class="category__image" src="img/catalog/product/item-3.png" alt="item-3">
            <div>
                <h5 class="category__title">Emerald</h5>
                <p class="card__subtitle">511 products</p>
            </div>   
        </a>
        <span>
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="12" height="12" x="0" y="0" viewBox="0 0 480.026 480.026" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path d="m475.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312-134.368 149.28c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/><path d="m267.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312L4.114 389.293c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/></g></svg>
        </span>
    `;

    const category4 = ` 
    <a href="sub-product-catalog.html" class="category__wrapper">
        <img class="category__image" src="img/catalog/product/item-4.png" alt="item-4">
        <div>
            <h5 class="category__title">Rubin</h5>
            <p class="card__subtitle">773 products</p>
        </div>
    </a>
    <span>
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="12" height="12" x="0" y="0" viewBox="0 0 480.026 480.026" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path d="m475.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312-134.368 149.28c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/><path d="m267.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312L4.114 389.293c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/></g></svg>
    </span>
    `;

    const category5 = ` 
    <a href="sub-product-catalog.html" class="category__wrapper">
        <img class="category__image" src="img/catalog/product/item-5.png" alt="item-5">
        <div>
            <h5 class="category__title">Fionite</h5>
            <p class="card__subtitle">206 products</p>
        </div>       
    </a>
    `;

    const categories = [

    {
        code: 'category1',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category-2',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category-3',
        url: 'index.html',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category-4',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category-5',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category-6',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category1-1',
        name: category2,
        parent: 'category1',
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category1-2',
        name: category2,
        parent: 'category1',
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category1-3',
        name: category2,
        parent: 'category1',
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category1-4',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-4',
    },
    {
        code: 'category1-5',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-5',
    },
    {
        code: 'category1-6',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-6',
    },
    {
        code: 'category1-7',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-7',
    },
    {
        code: 'category1-8',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-8',
    },
    {
        code: 'category1-9',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-9',
    },
    {
        code: 'category1-10',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-10',
    },
    {
        code: 'category1-11',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-11',
    },
    {
        code: 'category1-12',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-12',
    },
    {
        code: 'category1-13',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-13',
    },

    {
        code: 'category1-14',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-14',
    },

    {
        code: 'category1-15',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-15',
    },
    {
        code: 'category-2-1',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-1',
    },
    {
        code: 'category-2-2',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-2',
    },
    {
        code: 'category-2-3',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-3',
    },
    {
        code: 'category-2-4',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-4',
    },
    {
        code: 'category-2-5',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-5',
    },
    {
        code: 'category-2-6',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-6',
    },
    {
        code: 'category-2-7',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-7',
    },
    {
        code: 'category-2-8',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-8',
    },
    {
        code: 'category-2-9',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-9',
    },
    {
        code: 'category-2-10',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-10',
    },
    {
        code: 'category-2-11',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-11',
    },
    {
        code: 'category-2-12',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-12',
    },
    {
        code: 'category-2-13',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-13',
    },
    {
        code: 'category-3-1',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-1',
    },
    {
        code: 'category-3-2',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-2',
    },
    {
        code: 'category-3-3',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-3',
    },
    {
        code: 'category-3-4',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-4',
    },
    {
        code: 'category-3-5',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-5',
    },
    {
        code: 'category-3-6',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-6',
    },
    {
        code: 'category-3-7',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-7',
    },
    {
        code: 'category-3-8',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-8',
    },
    {
        code: 'category-3-9',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-9',
    },
    {
        code: 'category-3-10',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-10',
    },
    {
        code: 'category-3-11',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-11',
    },
    {
        code: 'category-3-12',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-12',
    },
    {
        code: 'category-3-13',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-13',
    },
    {
        code: 'category-4-1',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-1',
    },
    {
        code: 'category-4-2',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-2',
    },
    {
        code: 'category-4-3',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-3',
    },
    {
        code: 'category-4-4',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-4',
    },
    {
        code: 'category-4-5',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-5',
    },
    {
        code: 'category-4-6',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-6',
    },
    {
        code: 'category-4-7',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-7',
    },
    {
        code: 'category-4-8',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-8',
    },
    {
        code: 'category-4-9',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-9',
    },
    {
        code: 'category-4-10',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-10',
    },
    {
        code: 'category-4-11',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-11',
    },
    {
        code: 'category-4-12',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-12',
    },
    {
        code: 'category-4-13',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-13',
    },
    {
        code: 'category-5-1',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-1',
    },
    {
        code: 'category-5-2',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-2',
    },
    {
        code: 'category-5-3',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-3',
    },
    {
        code: 'category-5-4',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-4',
    },
    {
        code: 'category-5-5',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-5',
    },
    {
        code: 'category-5-6',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-6',
    },
    {
        code: 'category-5-7',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-7',
    },
    {
        code: 'category-5-8',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-8',
    },
    {
        code: 'category-5-9',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-9',
    },
    {
        code: 'category-5-10',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-10',
    },
    {
        code: 'category-5-11',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-11',
    },
    {
        code: 'category-5-12',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-12',
    },
    {
        code: 'category-5-13',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-13',
    },
    {
        code: 'category-6-1',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-1',
    },
    {
        code: 'category-6-2',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-2',
    },
    {
        code: 'category-6-3',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-3',
    },
    {
        code: 'category-6-4',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-4',
    },
    {
        code: 'category-6-5',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-5',
    },
    {
        code: 'category-6-6',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-6',
    },
    {
        code: 'category1-1-1',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-1',
    },
    {
        code: 'category1-1-2',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-2',
    },
    {
        code: 'category1-1-3',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-3',
    },
    {
        code: 'category1-1-4',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-4',
    },
    {
        code: 'category1-1-5',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-5',
    },
    {
        code: 'category1-1-6',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-6',
    },
    {
        code: 'category1-1-7',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-7',
    },
     {
        code: 'category1-1-8',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-8',
    },
    {
        code: 'category1-1-9',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-9',
    },
    {
        code: 'category1-1-6',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-6',
    },
    {
        code: 'category1-1-10',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-10',
    },
    {
        code: 'category1-1-11',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-11',
    },
    {
        code: 'category1-1-12',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-12',
    },
    {
        code: 'category1-1-13',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-13',
    },
    {
        code: 'category1-1-14',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-14',
    },
    {
        code: 'category1-1-15',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-15',
    },
    {
        code: 'category1-1-16',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-16',
    },
    {
        code: 'category1-2-1',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-1',
    },
    {
        code: 'category1-2-2',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-2',
    },
    {
        code: 'category1-2-3',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-3',
    },
    {
        code: 'category1-2-4',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-4',
    },
     {
        code: 'category1-2-5',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-5',
    },
    {
        code: 'category1-2-6',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-6',
    },
    {
        code: 'category1-3-1',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-1',
    },
    {
        code: 'category1-3-2',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-2',
    },
    {
        code: 'category1-3-3',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-3',
    },
    {
        code: 'category1-3-4',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-4',
    },
    {
        code: 'category1-3-5',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-5',
    },
    {
        code: 'category1-3-6',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-6',
    },
    {
        code: 'category1-4-1',
        name: category3,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-1',
    },
    {
        code: 'category1-4-2',
        name: category1,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-2',
    },
    {
        code: 'category1-4-3',
        name: category1,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-3',
    },
    {
        code: 'category1-4-4',
        name: category1,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-4',
    },
    {
        code: 'category1-4-5',
        name: category1,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-5',
    },
    {
        code: 'category1-4-6',
        name: category1,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-6',
    },
    {
        code: 'category1-5-1',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-1',
    },
    {
        code: 'category1-5-2',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-2',
    },
    {
        code: 'category1-5-3',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-3',
    },
    {
        code: 'category1-5-4',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-4',
    },
    {
        code: 'category1-5-5',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-5',
    },
    {
        code: 'category1-5-6',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-6',
    },
    {
        code: 'category1-6-1',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-1',
    },
    {
        code: 'category1-6-2',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-2',
    },
    {
        code: 'category1-6-3',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-3',
    },
    {
        code: 'category1-6-4',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-4',
    },
    {
        code: 'category1-6-5',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-5',
    },
    {
        code: 'category1-6-6',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-6',
    },
    {
        code: 'category-2-2-2',
        name: category1,
        parent: 'category-2-2',
        url: 'index.html?category=category-2-2-2',
    },
    {
        code: 'category-3-3-3',
        name: category1,
        parent: 'category-3-3',
        url: 'index.html?category=category-3-3-3',
    },
    {
        code: 'category-4-4-4',
        name: category3,
        parent: 'category-4-4',
        url: 'index.html?category=category-4-4-4',
    },
    {
        code: 'category1-1-1-1',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-1',
    },
    {
        code: 'category1-1-1-2',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-2',
    },
    {
        code: 'category1-1-1-3',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-3',
    },
    {
        code: 'category1-1-1-4',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-4',
    },
    {
        code: 'category1-1-1-5',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-5',
    },
    {
        code: 'category1-1-1-6',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-6',
    },
    {
        code: 'category1-1-1-7',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-7',
    },
    {
        code: 'category1-1-1-8',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-8',
    },
    {
        code: 'category1-1-1-9',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-9',
    },
    {
        code: 'category1-1-1-10',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-10',
    },
    {
        code: 'category1-1-1-11',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-11',
    },
    {
        code: 'category1-1-1-12',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-12',
    },
    {
        code: 'category1-1-1-13',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-13',
    },
    {
        code: 'category1-1-1-14',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-14',
    },
    {
        code: 'category1-1-1-15',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-15',
    },
    {
        code: 'category-2-2-2-2',
        name: category1,
        parent: 'category-2-2-2',
        url: 'index.html?category=category-2-2-2-2',
    },
    {
        code: 'category-3-3-3-3',
        name: category4,
        parent: 'category-3-3-3',
        url: 'index.html?category=category-3-3-3-3',
    },
    {
        code: 'category-1-1-1-1-1',
        name: category5,
        parent: 'category-1-1-1-1',
        url: 'index.html?category=category-1-1-1-1-1',
    },
    {
        code: 'category1-1-1-1-2',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-2',
    },
    {
        code: 'category1-1-1-1-3',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-3',
    },
    {
        code: 'category1-1-1-1-4',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-4',
    },
    {
        code: 'category1-1-1-1-5',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-5',
    },
    {
        code: 'category1-1-1-1-6',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-6',
    },
    {
        code: 'category1-1-1-1-7',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-7',
    },
    
    {
        code: 'category1-1-1-1-8',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-8',
    },

    {
        code: 'category1-1-1-1-9',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-9',
    },

    {
        code: 'category1-1-1-1-10',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-10',
    },

    {
        code: 'category1-1-1-1-11',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-11',
    },

    {
        code: 'category1-1-1-1-12',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-12',
    },

    {
        code: 'category1-1-1-1-13',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-13',
    },

    {
        code: 'category1-1-1-1-14',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-14',
    },

    {
        code: 'category1-1-1-1-15',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-15',
    },

    {
        code: 'category1-1-1-1-16',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-16',
    },

    {
        code: 'category1-1-1-1-17',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-17',
    },

    {
        code: 'category1-1-1-1-18',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-18',
    },
];

function createColumn(categories, counter) {
    const wrapper = document.querySelector('.categories')
    // даем Date атрибут counter //
    const column = document.createElement("li")
    column.classList.add("category__list")
    column.classList.add(`column-${counter}`)

    for (let category of categories) { 
        column.appendChild(categoryItem(category))
    }

    wrapper.append(column)
    return column
}

function categoryItem(category) {
    const categoryItem = document.createElement("a")
    categoryItem.classList.add("category__item");
    categoryItem.dataset.code = category.code
    categoryItem.dataset.url = category.url
    categoryItem.innerHTML = category.name
    
    categoryItem.addEventListener('click', (e) => { 
        let url = ''
        if (!e.target.classList.contains('category__item')) {
            url = e.target.closest('.category__item').dataset.url
        } else {
            url = e.target.dataset.url
        }
        window.location.href = url
    })

    categoryItem.addEventListener('mouseover', (e) => {
        let code = ''
        if (!e.target.classList.contains('category__item')) {
            code = e.target.closest('.category__item').dataset.code
        } else {
            code = e.target.dataset.code
        }
        // заменнить Date атрибут при скрытии последней колонки // 
        removeColumns(code)

        if (columntExists(code)) {
            return
        }
        const items = categories.filter(category => category.parent === code)
        const columnsSize = document.querySelectorAll('.category__list').length
        const column = createColumn(items, columnsSize + 1)
        column.dataset.parent = code
        const wrapper = document.querySelector('.categories')

        
        if (columnsSize >= 1) {
            wrapper.classList.add('categories-view-column-1');
        } else {
            wrapper.classList.remove('categories-view-column-1');
        }

        if (columnsSize >= 2) {
            wrapper.classList.add('categories-view-column-2');
        } else {
            wrapper.classList.remove('categories-view-column-2');
        }

        if (columnsSize >= 3) {
            wrapper.classList.add('categories-view-column-3');
        }  else {
            wrapper.classList.remove('categories-view-column-3');
        }

        if (columnsSize >= 4) {
            wrapper.classList.add('categories-view-column-4');
        }  else {
            wrapper.classList.remove('categories-view-column-4');
        }

        if (columnsSize >= 5) {
            wrapper.classList.add('categories-view-column-5');
        }  else {
            wrapper.classList.remove('categories-view-column-5');
        }
    })
    
    return categoryItem
}

function removeColumns(code, columns = []) {
    const category = categories.find(c => c.code === code)
    
    if (category.parent !== null) {
        columns.push(category.parent)
        removeColumns(category.parent, columns)
    } else {
        if (!columns.length) { // select only parent id
            getColumns().forEach(e => e.remove());
        } else {
            for (let col of getColumns()) {
                if (!columns.includes(col.dataset.parent)) {
                    col.remove()
                }
            }
        }
    }

    
}

function getColumns() {
    return document.querySelectorAll('.category__list[data-parent]')   
}

function columntExists(code) {
    const columns = getColumns();
    for (let column of columns) {
        if (column.dataset.parent == code) {
            return true
        }
    }

    return false
}

    createColumn(categories.filter(category => category.parent === null), 1);
    
// categories__modal

const categoriesButton = document.querySelector('.categories__button');
const categoriesModal = document.querySelector('.categories__modal');

categoriesButton.addEventListener('click', () => {  
    categoriesModal.classList.add('active');
    overlayActive();
    asideMain.classList.add('index');
})


// modal categories //

const categories1 = document.querySelector('.categories-1');
const categories2 = document.querySelector('.categories-2');
const categories3 = document.querySelector('.categories-3');

const categoriesList = document.querySelectorAll('.categories-mobile__wrapper');

categoriesList.forEach((categoryList) => {
    const category1 = categoryList.querySelectorAll('.category-1');
    const category2 = categoryList.querySelectorAll('.category-2');
   
    category1.forEach((cat1) => {   
        cat1.addEventListener('click', () => {
            categories1.style.display = "none";
            categories2.style.display = "block"
        })

        const backButton = categories2.querySelector('.back__button');
        backButton.addEventListener('click', () => {
            categories1.style.display = "block";
            categories2.style.display = "none";
        })
    })

    category2.forEach((cat2) => {   
        cat2.addEventListener('click', () => {
            categories2.style.display = "none";
            categories3.style.display = "block"
        })

        const backButton = categories3.querySelector('.back__button');
        backButton.addEventListener('click', () => {
            categories2.style.display = "block";
            categories3.style.display = "none";
        })
    })
})
}
const orderContent = document.querySelector('.basket__block');
const orderButton = document.querySelector('.draw__button');
const generateBlock = document.querySelector('.Querying__block');

if(window.location.toString().indexOf('order-create.html')>0) {
    const orderFinal = () => {
        orderContent.style.display = 'none';
        generateBlock.style.display = 'flex';
    }
    
    orderButton.addEventListener('click', () => {
        orderFinal();
    })
}

if(window.location.toString().indexOf('order-request.html')>0) {
    const orderFinal = () => {
        orderContent.style.display = 'none';
        generateBlock.style.display = 'flex';
    }
    
    orderButton.addEventListener('click', () => {
        orderFinal();
    })
}
const tdLink = document.querySelectorAll('.td__link');

tdLink.forEach((link) => {
    link.addEventListener('click', () => {
        location.href = 'appeal.html';
    })
})

const customLink = document.querySelectorAll('.custom-link');

customLink.forEach((custom) => {
    custom.addEventListener('click', () => {
        location.href = 'appeal.html';
    })
})

const productCardLink = document.querySelectorAll('.product-card__link');

productCardLink.forEach((card) => {
    card.addEventListener('click', () => {
        location.href = 'card-product.html';
    })
})

const cardContent = document.querySelectorAll('.item__name');

cardContent.forEach((card) => {
    card.addEventListener('click', () => {
        location.href = 'card-product.html';
    })
})

const cellLink = document.querySelectorAll('.cell__link');

cellLink.forEach((cell) => {
    cell.addEventListener('click', () => {
        location.href = 'card-product.html';
    })
})


const optionLink = document.querySelectorAll('.option__wrapper');

optionLink.forEach((option) => {
    option.addEventListener('click', () => {
        location.href = 'product-search.html';
    })
})

// const loginLink = document.querySelectorAll('.login__link');

// loginLink.forEach((login) => {
//     login.addEventListener('click', () => {
//         location.href = 'profile-select.html';
//     })
// })

const profileLink = document.querySelectorAll('.profile-selection__link');

profileLink.forEach((profile) => {
    profile.addEventListener('click', () => {
        location.href = 'index.html';
    })
})

const profileItem = document.querySelectorAll('.profile__item');

profileItem.forEach((profItem) =>{
    profItem.addEventListener('click', () => {
        location.href = 'details-organization.html';
    })
})

const detailsRepresentativeLink = document.querySelectorAll('.details-representative__link');

detailsRepresentativeLink.forEach((details) => {
    details.addEventListener('click', () => {
        location.href = 'details-representative.html';
    })
})
const listBlock = document.querySelectorAll('.list__block');

listBlock.forEach((block) => {
    var list = block.querySelector('.hidden__list');
    var showButton = block.querySelector('.show__button');
    var text = block.querySelector('.text');
    showButton.addEventListener('click', () => {
        list.classList.toggle('show');
        showButton.classList.toggle('rotate');
        text.innerHTML = (text.innerHTML === 'Collapse') ? text.innerHTML = 'Expand' : text.innerHTML = 'Collapse';
    })
})
const mainModal = document.querySelectorAll('.modal-window');

mainModal.forEach((main) => {
    const closeModal = main.querySelectorAll('.modal__hidden');

    // closeModal.addEventListener('click', () => {
    //     main.classList.remove('active');
    //     overlayHide();
    //     asideMain.classList.remove('index');
    // })

    closeModal.forEach((close) => {
        close.addEventListener('click', () => {
            main.classList.remove('active');
            overlayHide();
            asideMain.classList.remove('index');
        })
    })
})


const orderSearch = document.querySelector('.order-search__modal');
const documentSearchButton = document.querySelector('.document-search');


if (document.getElementsByClassName('document-search').length > 0) {
    documentSearchButton.addEventListener('click', () => {
        orderSearch.classList.toggle('active');
    })
}

if (document.getElementsByClassName('configure__button').length > 0) {
    const configureModal = document.querySelector('.configure__modal');
    const configureButton = document.querySelector('.configure__button');

    configureButton.addEventListener('click', () => {
        configureModal.classList.toggle('active');
        overlayActive();
    })
}

if (document.getElementsByClassName('sub-modal__button').length > 0) {
    const subModal = document.querySelector('.sub-modal');
    const subModalButton = document.querySelectorAll('.sub-modal__button');

    subModalButton.forEach((subbutton) => {
        subbutton.addEventListener('click', () => {
            subModal.classList.toggle('active');
            overlayActive();
        })
    })
}

if (document.getElementsByClassName('sort-mobile').length > 0) {
    const sortModal = document.querySelector('.sort-mobile__modal');
    const sortMobileButton = document.querySelectorAll('.sort-mobile');

    sortMobileButton.forEach((sortbutton) => {
        sortbutton.addEventListener('click', () => {
            sortModal.classList.toggle('active');
            overlayActive();
        })
    })
}

if (document.getElementsByClassName('delivery-conditions___modal').length > 0) {
    const deliveryConditionsModal = document.querySelector('.delivery-conditions___modal');
    const deliveryButton = document.querySelector('.delivery__button');

    deliveryButton.addEventListener('click', () => {
        deliveryConditionsModal.classList.toggle('active');
        overlayActive();
    })
}

if (document.getElementsByClassName('delivery-payment___modal').length > 0) {
    const deliveryPaymentModal = document.querySelector('.delivery-payment___modal');
    const deliveryButton = document.querySelector('.payment__button');

    deliveryButton.addEventListener('click', () => {
        deliveryPaymentModal.classList.toggle('active');
        overlayActive();
    })
}
if (document.getElementsByClassName('slider-track').length > 0) {
    window.onload = function () {
        slideOne();
        slideTwo();
      };
      
      let sliderOne = document.getElementById("slider-1");
      let sliderTwo = document.getElementById("slider-2");
      let displayValOne = document.getElementById("range1");
      let displayValTwo = document.getElementById("range2");
      let minGap = 0;
      let sliderTrack = document.querySelector(".slider-track");
      let sliderMaxValue = document.getElementById("slider-1").max;
  
      function slideOne() {
        if (parseInt(sliderTwo.value) - parseInt(sliderOne.value) <= minGap) {
          sliderOne.value = parseInt(sliderTwo.value) - minGap;
        }
        displayValOne.textContent = sliderOne.value;
        fillColor();
      }
      function slideTwo() {
        if (parseInt(sliderTwo.value) - parseInt(sliderOne.value) <= minGap) {
          sliderTwo.value = parseInt(sliderOne.value) + minGap;
        }
        displayValTwo.textContent = sliderTwo.value;
        fillColor();
      }
      function fillColor() {
        percent1 = (sliderOne.value / sliderMaxValue) * 100;
        percent2 = (sliderTwo.value / sliderMaxValue) * 100;
      }
      
}

const popups1 = document.querySelector('.popup__switch');
const popups2 = document.querySelector('.popup__delete');
const popups3 = document.querySelector('.popup__done');
const popupsButtons1 = document.querySelectorAll('.popup-switch__button');
const popupsButtons2 = document.querySelectorAll('.popup-delete__button');
const popupsButtons3 = document.querySelectorAll('.popup-done__button');

popupsButtons1.forEach((btn1) => {
    btn1.addEventListener('click', () => {
        popups1.classList.toggle('active');
        overlayActive();
    })
});

popupsButtons2.forEach((btn2) => {
    btn2.addEventListener('click', () => {
        popups2.classList.toggle('active');
        overlayActive();
    })

    const confirmationButton = popups2.querySelector('.confirmation__button');

    confirmationButton.addEventListener('click', () => {
        popups2.classList.add('confirmation'); 
    })
});

popupsButtons3.forEach((btn3) => {
    btn3.addEventListener('click', () => {
        popups3.classList.toggle('active');
        overlayActive();
    })
});
// const scrollUp = document.querySelector('.scroll-up');

// const displayButton = () => {
//   window.addEventListener('scroll', () => {
  
//     if (window.scrollY > 200) {
//         scrollUp.classList.toggle('active')
//     } else {
//         scrollUp.classList.remove('active')
//     }
//   });
// };

// const scrollToTop = () => {
//     scrollUp.addEventListener("click", () => {
//     window.scroll({
//       top: 0,
//       left: 0,
//       behavior: 'smooth'
//     }); 
//   });
// };

// displayButton();
// scrollToTop();


// back to top // 

const backToTop = document.querySelector('.scroll-up');
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
        backToTop.classList.add('active');
    } else {
        backToTop.classList.remove('active');
    }
}

function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}


// scroll header //

if (document.getElementsByClassName('header__wrapper').length > 0) {
    const headerWrapper = document.querySelector('.header__wrapper');
    const body = document.body;
    const scrollUp = "scroll-up";
    const scrollDown = "scroll-down";
    let lastScroll = 0;

    window.addEventListener("scroll", () => {
        const currentScroll = window.pageYOffset;
        if (currentScroll <= 0) {
            headerWrapper.classList.remove('scroll');
            return;
        }
    
        if (currentScroll > lastScroll && !body.classList.contains(scrollDown)) {
        
        headerWrapper.classList.add('scroll');
        //   body.classList.remove(scrollUp);
        //   body.classList.add(scrollDown);
        

        } 
    });
}
if (document.getElementsByClassName('all-checkbox').length > 0) {
    
    document.querySelector(".all-checkbox").addEventListener('click', function(e) {

    [].forEach.call(document.querySelectorAll('.checkout__input'), c => c.checked = this.checked);
        
    }); 

    const downloadSelected = document.querySelector('.download-selected');
    const checkbox = document.querySelectorAll('.checkout__input')
    
    checkbox.forEach((check) => {
        check.addEventListener('change', () => {
            if (
                document.querySelectorAll('.checkout__input:checked').length >= 2
            ){
                downloadSelected.classList.add('active');
            } else {
                downloadSelected.classList.remove('active');
            }
             
        })
    })
}
const editFieldsButton = document.querySelector(".edit-fields__button");
const fields = document.querySelectorAll(".form__wrapper");
const saveChanges = document.querySelector('.save-changes');
const profilePhoto = document.querySelector('.profile__photo');
const repeatPassword = document.querySelector('.repeat-password');
const editingButton = document.querySelector('.editing__button');
const applyButton = document.querySelector('.apply__button');
const detailsSelect = document.querySelector('.details-select');
const profileEditDel = document.querySelectorAll('.profile__edit-del');

if (document.getElementsByClassName('edit-fields__button').length > 0) {
    editFieldsButton.addEventListener('click', () => {
        fields.forEach((field) => {
            const inputField = field.querySelector('.form__input')
            field.classList.remove('disable');
            inputField.removeAttribute("readonly");
            saveChanges.classList.add('active');
            editFieldsButton.classList.add('hide');
        })

        profilePhoto.classList.add('add');
        repeatPassword.classList.add('show');
    })
}

if (document.getElementsByClassName('editing__button').length > 0) {
    editingButton.addEventListener('click', () => {
        fields.forEach((field) => {
            field.classList.remove('disable');
        })

        editingButton.classList.add('hidden');
        applyButton.classList.add('active');
        detailsSelect.classList.add('edit');

        profileEditDel.forEach((editdel) => {
            editdel.classList.add('edit');
        })
    })

    applyButton.addEventListener('click', () => {
        fields.forEach((field) => {
            field.classList.add('disable');
        })

        editingButton.classList.remove('hidden');
        applyButton.classList.remove('active');
        detailsSelect.classList.remove('edit');

        profileEditDel.forEach((editdel) => {
            editdel.classList.remove('edit');
        })
    })
}
if (document.getElementsByClassName('sum-filter').length > 0) {
    const sumFilter = document.querySelectorAll('.sum-filter');
    
    sumFilter.forEach((sumfil) => {
        const sumInput = sumfil.querySelector('.sum__input');
        const closeSum = sumfil.querySelector('.close-sum');

        sumInput.oninput = () => {
            sumfil.classList.add('active');
        }

        closeSum.addEventListener('click', () => {
            sumInput.value = '';
            sumfil.classList.remove('active');
        })
        
    })
}

// if (document.getElementsByClassName('main__form').length > 0) {
//     const mainForm = document.querySelectorAll('.main__form');

//     mainForm.forEach((form) => {
        
//         function showError(input) {
//             const formControl = input.parentElement;
//             formControl.className = 'form__wrapper error';
//         }

//         function showSucces(input) {
//             const formControl = input.parentElement;
//             formControl.className = 'form__wrapper success';
//         }

//         function checkRequired(inputArr) {
//             inputArr.forEach(function(input){
//                 if(input.value.trim() === ''){
//                     showError(input,`${getFieldName(input)} is required`)
//                 }else {
//                     showSucces(input);
//                 }
//             });
//         }

//         function checkLength(input, min) {
//             if(input.value.length < min) {
//                 showError(input, `${getFieldName(input)} must be at least ${min} characters`);
//             } else {
//                 showSucces(input);
//             }
//         }

//         function getFieldName(input) {
//             return input.className.charAt(0).toUpperCase() + input.className.slice(1);
//         }
        
//         const emailWrapper = form.querySelectorAll('.form__wrapper');

//         emailWrapper.forEach((wrap) => {
//             const showPassword = wrap.querySelector('.show__password');
//             const passwordInput = wrap.querySelector('.password__input');

//         if(showPassword) {
//             showPassword.addEventListener('click', () => {
//                 passwordInput.setAttribute('type', 'text');
//                 showPassword.classList.add('show');
//             })
//         } 
//         })

//         form.addEventListener('submit',function(e) {
//             e.preventDefault();
            
//             const symbols = form.querySelectorAll('.symbols');

//             symbols.forEach((sym) => {
//                 checkRequired([sym]);
//                 checkLength(sym,3);
//             })

//             // checkRequired([username]);
//             // checkLength(username,3,15);
//         });
//     })
// }

if (document.getElementsByClassName('authorization__form').length > 0) {
    const authorizationForm = document.querySelector('.authorization__form');
    const formInputs = authorizationForm.querySelectorAll('.form__input');
    const loginButton = authorizationForm.querySelector('.login__button');
    const emailWrapper = authorizationForm.querySelector('.email__wrapper');
    const passwordWrapper = authorizationForm.querySelector('.password__wrapper');
    const showPassword = authorizationForm.querySelector('.show__password')
    const passwordInput = authorizationForm.querySelector('.password__input');
    const emailInput = document.querySelector('.email__input');
    const validFormArr = [];

    
    if (emailInput.hasAttribute("data-reg")) {
        emailInput.setAttribute("is-valid", "false");
        validFormArr.push(emailInput);
        emailInput.addEventListener("input", inputHandler)
    }

    if (passwordInput.hasAttribute("data-reg")) {
        passwordInput.setAttribute("is-valid", "false");
        validFormArr.push(passwordInput);
        passwordInput.addEventListener("input", passwordHandler)
    }
   

    function inputHandler({ target }) {
        const inputValue = target.value;
        const inputReg = target.getAttribute("data-reg");
        const reg = new RegExp(inputReg);
        if (reg.test(inputValue)) {
            target.setAttribute("is-valid", "true");
            emailWrapper.classList.add('success')
        } else {
            target.setAttribute("is-valid", "false");
            emailWrapper.classList.add('error')
        }
    }

    function passwordHandler({ target }) {
        const inputValue = target.value;
        const inputReg = target.getAttribute("data-reg");
        const reg = new RegExp(inputReg);
        if (reg.test(inputValue)) {
            target.setAttribute("is-valid", "true");
            passwordWrapper.classList.add('success')
        } else {
            target.setAttribute("is-valid", "false");
            passwordWrapper.classList.add('error')
        }
    }

    showPassword.addEventListener('click', ()=> {
        showPassword.classList.add('show');
        passwordInput.type="text"; 
    })

    loginButton.addEventListener('click', () => {
        const inputsLength = validFormArr.length;
        const inputsOnlyValid = validFormArr.filter((input) => input.getAttribute('is-valid') === "true");

        if (inputsLength !== inputsOnlyValid.length) {
            emailWrapper.classList.add('error')
            passwordWrapper.classList.add('error')
        } else {

            location.href = 'profile-select.html';
          
        }

    })
   
}
if (document.getElementsByClassName('dial-in__form').length > 0) {
    const dialInForm = document.querySelector('.dial-in__form');
    const phoneForm = document.querySelector('.phone__form');
    const dialInButton = document.querySelector('.dial-in__button');

    const OpenDialForm = () => {
        phoneForm.classList.toggle('hide');
        dialInForm.classList.toggle('active');
    }

    dialInButton.addEventListener('click', () => {
        OpenDialForm();
    })


    let in1 = document.getElementById('otc-1'),
    ins = document.querySelectorAll('input[type="number"]'),
	 splitNumber = function(e) {
		let data = e.data || e.target.value; // Chrome doesn't get the e.data, it's always empty, fallback to value then.
		if ( ! data ) return; // Shouldn't happen, just in case.
		if ( data.length === 1 ) return; // Here is a normal behavior, not a paste action.
		
		popuNext(e.target, data);
		//for (i = 0; i < data.length; i++ ) { ins[i].value = data[i]; }
	},
	popuNext = function(el, data) {
		el.value = data[0]; // Apply first item to first input
		data = data.substring(1); // remove the first char.
		if ( el.nextElementSibling && data.length ) {
			// Do the same with the next element and next data
			popuNext(el.nextElementSibling, data);
		}
	};

    ins.forEach(function(input) {
	/**
	 * Control on keyup to catch what the user intent to do.
	 * I could have check for numeric key only here, but I didn't.
	 */
	input.addEventListener('keyup', function(e){
		// Break if Shift, Tab, CMD, Option, Control.
		if (e.keyCode === 16 || e.keyCode == 9 || e.keyCode == 224 || e.keyCode == 18 || e.keyCode == 17) {
			 return;
		}
		
		// On Backspace or left arrow, go to the previous field.
		if ( (e.keyCode === 8 || e.keyCode === 37) && this.previousElementSibling && this.previousElementSibling.tagName === "INPUT" ) {
			this.previousElementSibling.select();
		} else if (e.keyCode !== 8 && this.nextElementSibling) {
			this.nextElementSibling.select();
		}
		
		// If the target is populated to quickly, value length can be > 1
		if ( e.target.value.length > 1 ) {
			splitNumber(e);
		}
	});
	
	/**
	 * Better control on Focus
	 * - don't allow focus on other field if the first one is empty
	 * - don't allow focus on field if the previous one if empty (debatable)
	 * - get the focus on the first empty field
	 */
	input.addEventListener('focus', function(e) {
		// If the focus element is the first one, do nothing
		if ( this === in1 ) return;
		
		// If value of input 1 is empty, focus it.
		if ( in1.value == '' ) {
			in1.focus();
		}
		
		// If value of a previous input is empty, focus it.
		// To remove if you don't wanna force user respecting the fields order.
		if ( this.previousElementSibling.value == '' ) {
			this.previousElementSibling.focus();
		}
	});
});

/**
 * Handle copy/paste of a big number.
 * It catches the value pasted on the first field and spread it into the inputs.
 */
in1.addEventListener('input', splitNumber);
}

if (document.getElementsByClassName('password-recovery__section').length > 0) {
	const stage1 = document.querySelector('.stage-1');
	const stage2 = document.querySelector('.stage-2');
	const submitEmail = document.querySelector('.submit-email');

	submitEmail.addEventListener('click', () => {
		stage1.classList.toggle('hidden');
		stage2.classList.toggle('active');
	})
}

if (document.getElementsByClassName('custom-scroll__table').length > 0) {
    const costumeTableMain = document.querySelector('.custom-scroll__table');
    const columnLast = document.querySelectorAll('.cell-sticky');
    const columnFirst = document.querySelectorAll('.cell__link');
    const rowHeader = document.querySelector('.custom-scroll-table__header');
    const lastColName = document.querySelector('.last-col-name');
    const rows = document.querySelectorAll('.row')

    rows.forEach((row) => {
        row.style.width = "2520px";
    })

    rowHeader.style.width = "2520px";

    costumeTableMain.addEventListener('scroll', function() {
        
        if (costumeTableMain.offsetWidth + costumeTableMain.scrollLeft >= costumeTableMain.scrollWidth) {
            columnLast.forEach((col) => {
                col.classList.remove('boxShadow');
            })

            columnFirst.forEach((col2) => {
                col2.classList.add('boxShadow');
            })

            lastColName.classList.remove('boxShadow');

            rowHeader.classList.add('boxShadow');

        } else {
            columnLast.forEach((col) => {
                col.classList.add('boxShadow');
            })

            columnFirst.forEach((col2) => {
                col2.classList.remove('boxShadow');
            })

            lastColName.classList.add('boxShadow');

            rowHeader.classList.remove('boxShadow');
        }

        if (costumeTableMain.offsetWidth + costumeTableMain.scrollLeft < costumeTableMain.scrollWidth) {
            columnFirst.forEach((col2) => {
                col2.classList.add('boxShadow');
            })

            rowHeader.classList.add('boxShadow');
        }

        if (this.scrollLeft == 0) {
            columnFirst.forEach((col2) => {
                col2.classList.remove('boxShadow');
            })

            rowHeader.classList.remove('boxShadow');
        }

        
    })

    reduceMenu.addEventListener('click', () => {
        costumeTableMain.classList.toggle('max-width');
    })
}