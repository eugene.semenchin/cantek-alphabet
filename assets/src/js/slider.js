var swiper = new Swiper(".offer__slider", {
    slidesPerView: 'auto',
    freeMode: true,
    loop: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
   
    breakpoints: {
        1: {
            slidesPerView: 1,
            spaceBetween: 24,
            resistanceRatio: 0.85,
            centeredSlides: true,
        },

        426: {
            slidesPerView: 'auto',
            spaceBetween: 14,
            centeredSlides: false,
        },

        576: {
            spaceBetween: 16,
            resistanceRatio: 0.85
        },

        768: {
            spaceBetween: 18,
            resistanceRatio: 0.85
        },

        1024: {
            spaceBetween: 24,
            resistanceRatio: 0,
            autoplay: false,
        }
    },

    navigation: {
        nextEl: '.next-offer',
        prevEl: '.prev-offer',
    },
});

var swiper = new Swiper(".offer__slider--mobile", {
    slidesPerView: 'auto',
    // freeMode: true,
    allowTouchMove: false,
    loop: true,
    autoplay: {
        delay: 1500,
        // disableOnInteraction: false,
    },
   
    breakpoints: {
        320: {
            spaceBetween: 14,
            resistanceRatio: 0.85,
        },

        576: {
            spaceBetween: 16,
            resistanceRatio: 0.85
        },

        768: {
            spaceBetween: 18,
            resistanceRatio: 0.85
        },

        1024: {
            spaceBetween: 24,
            resistanceRatio: 0,
            autoplay: false,
        }
    },

    navigation: {
        nextEl: '.next-offer',
        prevEl: '.prev-offer',
    },
});

var swiper = new Swiper(".current__slider", {
    slidesPerView: 'auto',
    freeMode: true,
    loop: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },

    breakpoints: {
        320: {
            spaceBetween: 14,
            resistanceRatio: 0.85,
        },

        576: {
            spaceBetween: 16,
            resistanceRatio: 0.85
        },

        768: {
            spaceBetween: 18,
            resistanceRatio: 0.85
        },

        1024: {
            spaceBetween: 24,
            resistanceRatio: 0,
            autoplay: false,
        }
    },

    navigation: {
        nextEl: '.next-current',
        prevEl: '.prev-current',
    },
});

var swiper = new Swiper(".card__slider-1", {
    loop: true,
    spaceBetween: 10,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesProgress: true,
    breakpoints: {
        576: {
            slidesPerView: 3,
        },

        768: {
            slidesPerView: 4,
        },
    }

  });
  var swiper2 = new Swiper(".card__slider-2", {
    loop: true,
    spaceBetween: 20,
    thumbs: {
      swiper: swiper,
    },

    navigation: {
        nextEl: '.next-card',
        prevEl: '.prev-card',
    },
});


const tableCard = document.querySelectorAll('.mobile-slider-card');

tableCard.forEach((card) => {
    card.querySelector('.slider--active').addEventListener('click', () => {
       card.classList.toggle('slider'); 
    })

    card.querySelector('.close__slider').addEventListener('click', () => {
        card.classList.remove('slider'); 
    })

    var swiper = new Swiper(card.querySelector('.table-card__slider'), {
        cssMode: true,
        centeredSlides: true,
        roundLengths: true,
        navigation: {
          nextEl: card.querySelector('.table-card-next'),
          prevEl: card.querySelector('.table-card-prev'),
        },
        pagination: {
          el: card.querySelector('.swiper-pagination'),
        },
        mousewheel: true,
        keyboard: true,
    });
})

