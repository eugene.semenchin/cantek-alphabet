// Основные табы //

const tabContainers = document.querySelectorAll('.tab-container');
const tabsScroll = document.querySelector('.section__tabs');

tabContainers.forEach((container) => {
	const tabButtons = container.querySelectorAll('[data-tab-target]');
	const tabContents = container.querySelectorAll('.tab__content');
	const nextTabButton = container.querySelector('.next__tab');
	const prevTabButton = container.querySelector('.prev__tab');
	
	if (tabButtons.length && tabContents.length) {
		tabButtons.forEach((tab) => {
			tab.addEventListener("click", () => {
				tabButtons.forEach((tab) => {
					tab.classList.remove("active");
				});
				
				tab.classList.add("active");
		
				tabContents.forEach((tabContent) => {
					tabContent.classList.remove("active");
				});
		
				const target = document.querySelector(tab.dataset.tabTarget);
				target.classList.add("active");
			});
		});
	}

	if (nextTabButton && prevTabButton) {	
		
		const currentTab = container.querySelectorAll('.section__tab');
		const currenttTabActive = container.querySelector('.section__tab.active');
		const currentNoTab = container.querySelector('.section__tab.no-active');
		const nextTab = currentTab.nextElementSibling;
		const prevTab = currentNoTab.previousElementSibling;
		const currentContent = container.querySelectorAll('.tab__content')
		const currentNoContent = container.querySelector('.tab__content.no-active')
		const nextContent = currentContent.nextElementSibling;
		const prevContent = currentNoContent.previousElementSibling;
		var i = 0;
		var q = 0;

		nextTabButton.addEventListener('click', () => {
			currentTab[i].classList.remove("active");
			i++;

			if (i >= currentTab.length) {
			i = 0;
			}

			currentTab[i].classList.add("active");

			currentContent[q].classList.remove("active");
			q++;

			if (q >= currentContent.length) {
			q = 0;
			}

			currentContent[q].classList.add("active");
		})

		prevTabButton.addEventListener('click', () => {
			currentTab[i].classList.remove("active");
			i--;

			if (i < 0) {
			i = currentTab.length - 1;
			}
			currentTab[i].classList.add("active");

			currentContent[q].classList.remove("active");
			q--;

			if (q < 0) {
			q = currentContent.length - 1;
			}
			currentContent[q].classList.add("active");
		})
	}
})

// Табы с общим Amountм //

const maintabs = document.querySelectorAll('.main__tabs');
maintabs.forEach((maintab) => {
	const subTabButtons = maintab.querySelectorAll('.main__tab');

	subTabButtons.forEach((subTabButton) => {
	subTabButton.addEventListener('click', function (event) {

		// снова прохоSTмся по Allм кнопкам чтобы Delete класс
			subTabButtons.forEach((sub) => {
		sub.classList.remove('active');
			})

			// добавляем класс только на ту кнопку, на которуб кликнули
		this.classList.add('active');
		
		let filter = this.dataset.filter;
		let trs = document.querySelectorAll('.subcontent');

		trs.forEach(function(tr) {
				// если фильтр "All"
				if (filter === 'all') {
					// удаляем All классы
					tr.classList.remove('hide');
					tr.classList.remove('show');
				} else { // если фильтр любой кроме "All"
					// если у карточки в списке классов есть нужный класс
					if (tr.classList.contains(filter + '__subcontent')) {
						// удаляем класс если он есть
						tr.classList.remove('hide');
						tr.classList.add('show');
					} else { // если карточка не фильтруемая
						//скрываем
						tr.classList.add('hide');
						tr.classList.remove('show');
					}
				}
			});
	})
	})
})

if (document.getElementsByClassName('mob-table-cards').length > 0) {
	function Tabs() {
		var bindAll = function() {
		  var menuElements = document.querySelectorAll('[data-tab]');
		  for(var i = 0; i < menuElements.length ; i++) {
			menuElements[i].addEventListener('click', change, false);
		  }
		}
	  
		var clear = function() {
		  var menuElements = document.querySelectorAll('[data-tab]');
		  for(var i = 0; i < menuElements.length ; i++) {
			menuElements[i].classList.remove('active');
			var id = menuElements[i].getAttribute('data-tab');
			document.getElementById(id).classList.remove('active');
		  }
		}
	  
		var change = function(e) {
		  clear();
		  e.target.classList.add('active');
		  var id = e.currentTarget.getAttribute('data-tab');
		  document.getElementById(id).classList.add('active');
		}
	  
		bindAll();
	  }
	  
	var connectTabs = new Tabs();

}

// Табы оформить //

const productTab = document.querySelector('.product-basket__tab');
const listRequestTab = document.querySelector('.list-request__tab');
const makeOrder = document.querySelector('.make-order');
const makeRequest = document.querySelector('.make-request');

if (document.getElementsByClassName('product-basket__tab').length > 0) {
	listRequestTab.addEventListener('click', () => {
		makeRequest.classList.add('active');
		makeOrder.classList.add('hidden');
	})
	
	productTab.addEventListener('click', () => {
		makeRequest.classList.remove('active');
		makeOrder.classList.remove('hidden');
	})
}








  
