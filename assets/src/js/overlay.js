// overlay // 

const overlay = document.querySelector('.overlay');
const body = document.querySelector('body');

const overlayActive = function () {
    overlay.classList.add('overlay--active');
    body.classList.add('none')
};

const overlayHide = function () {
    overlay.classList.remove('overlay--active');
    body.classList.remove('none')
}