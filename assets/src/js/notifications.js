const notificationButtons = document.querySelectorAll('.notification__button');
const notificationsModal = document.querySelector('.notifications__modal');
const notificationsClose = document.querySelector('.notifications-modal__close');

if (document.getElementsByClassName('notification__button').length > 0) {
    const notificationModalActive = function () {
        notificationsModal.classList.toggle('active');
    }
    
    const notificationsModalClose = function () {
        notificationsModal.classList.remove('active');
    }
    
    notificationButtons.forEach(function (i) {
        i.addEventListener("click", function () {
            notificationModalActive();
            overlayActive();
        });
    });
    
    notificationsClose.addEventListener('click', function () {
        notificationsModalClose();
        overlayHide();
    })
}


