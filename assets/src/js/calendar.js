const calendar = document.querySelector('.calendar__modal');
const calendarInput = document.querySelectorAll('.calendar__input');
// const daysTag = document.querySelector(".days");
// const currentDate = document.querySelector(".data__title");
// const prevNextIcon = document.querySelectorAll(".current-date .data__button");
const actCalendar = document.querySelector('.act-calendar__modal');
const actCalendarButton = document.querySelector('.act__button')
const calendarTable = document.querySelectorAll('.calendar__table');

if (document.getElementsByClassName('calendar__input').length > 0) {

    calendarTable.forEach((table) => {
        const daysTag = table.querySelector(".days");
        const currentDate = table.querySelector(".data__title");
        const prevNextIcon = table.querySelectorAll(".current-date .data__button");
        
        let currYear = new Date().getFullYear();
        let currMonth = new Date().getMonth();
    
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    
        const renderCalendar = () => {
            const date = new Date(currYear, currMonth, 1);
            let firstDayofMonth = date.getDay();
            let lastDateofMonth = new Date(currYear, currMonth + 1, 0).getDate();
            let lastDayofMonth = new Date(currYear, currMonth, lastDateofMonth).getDay();
            let lastDateofLastMonth = new Date(currYear, currMonth, 0).getDate();
    
            let liTag = "";
    
            for (let i = firstDayofMonth; i > 0; i--) {
                liTag += `<li class="day inactive">${lastDateofLastMonth - i + 1}</li>`;
            }
    
            for (let i = 1; i <= lastDateofMonth; i++) {
                let isToday = i === new Date().getDate() && currMonth === new Date().getMonth() && currYear === new Date().getFullYear() ? "today" : "";
                liTag += `<li class="day ${isToday}">${i}</li>`;
            }
    
            for (let i = lastDayofMonth; i < 6; i++) {
                liTag += `<li class="day inactive">${i - lastDayofMonth + 1}</li>`
            }
    
            currentDate.innerText = `${months[currMonth]} ${currYear}`;
            daysTag.innerHTML = liTag;
        };
    
        renderCalendar();
    
        prevNextIcon.forEach(icon => {
            icon.addEventListener("click", () => {
                currMonth = icon.id === "prev" ? currMonth - 1 : currMonth + 1;
    
                if (currMonth < 0 || currMonth > 11) {
                    currYear = icon.id === "prev" ? currYear - 1 : currYear + 1;
                    currMonth = currMonth < 0 ? 11 : 0;
                }
    
                renderCalendar();
            });
        });
    })

    const calendarShow = () => {
        calendar.classList.toggle('active');
    }


    calendarInput.forEach((inputcal) => {
        inputcal.addEventListener('click', () => {
            calendarShow();
            overlayActive();
        })
    })
}

if (document.getElementsByClassName('act__button').length > 0) {
    actCalendarButton.addEventListener('click', () => {
        actCalendar.classList.toggle('active');
        overlayActive();
    }) 
}


