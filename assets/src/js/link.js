const tdLink = document.querySelectorAll('.td__link');

tdLink.forEach((link) => {
    link.addEventListener('click', () => {
        location.href = 'appeal.html';
    })
})

const customLink = document.querySelectorAll('.custom-link');

customLink.forEach((custom) => {
    custom.addEventListener('click', () => {
        location.href = 'appeal.html';
    })
})

const productCardLink = document.querySelectorAll('.product-card__link');

productCardLink.forEach((card) => {
    card.addEventListener('click', () => {
        location.href = 'card-product.html';
    })
})

const cardContent = document.querySelectorAll('.item__name');

cardContent.forEach((card) => {
    card.addEventListener('click', () => {
        location.href = 'card-product.html';
    })
})

const cellLink = document.querySelectorAll('.cell__link');

cellLink.forEach((cell) => {
    cell.addEventListener('click', () => {
        location.href = 'card-product.html';
    })
})


const optionLink = document.querySelectorAll('.option__wrapper');

optionLink.forEach((option) => {
    option.addEventListener('click', () => {
        location.href = 'product-search.html';
    })
})

// const loginLink = document.querySelectorAll('.login__link');

// loginLink.forEach((login) => {
//     login.addEventListener('click', () => {
//         location.href = 'profile-select.html';
//     })
// })

const profileLink = document.querySelectorAll('.profile-selection__link');

profileLink.forEach((profile) => {
    profile.addEventListener('click', () => {
        location.href = 'index.html';
    })
})

const profileItem = document.querySelectorAll('.profile__item');

profileItem.forEach((profItem) =>{
    profItem.addEventListener('click', () => {
        location.href = 'details-organization.html';
    })
})

const detailsRepresentativeLink = document.querySelectorAll('.details-representative__link');

detailsRepresentativeLink.forEach((details) => {
    details.addEventListener('click', () => {
        location.href = 'details-representative.html';
    })
})

