// текст //
if (document.getElementsByClassName('categories__modal').length > 0) {

    const category1 = ` 
        <a href="sub-product-catalog.html" class="category__wrapper">
            <img class="category__image" src="img/catalog/product/item-1.png" alt="item-1">
            <h5 class="category__title">Crystal</h5>
        </a>
        <span>
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="12" height="12" x="0" y="0" viewBox="0 0 480.026 480.026" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path d="m475.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312-134.368 149.28c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/><path d="m267.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312L4.114 389.293c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/></g></svg>
        </span>
    `;

    const category2 = ` 
        <a href="sub-product-catalog.html" class="category__wrapper">
            <img class="category__image" src="img/catalog/product/item-2.png" alt="item-2">
            <div>
                <h5 class="category__title">Diamond</h5>
                <p class="card__subtitle">173 products</p>
            </div>
        </a>
        <span>
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="12" height="12" x="0" y="0" viewBox="0 0 480.026 480.026" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path d="m475.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312-134.368 149.28c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/><path d="m267.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312L4.114 389.293c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/></g></svg>
        </span>
    `;

    const category3 = ` 
        <a href="sub-product-catalog.html" class="category__wrapper">
            <img class="category__image" src="img/catalog/product/item-3.png" alt="item-3">
            <div>
                <h5 class="category__title">Emerald</h5>
                <p class="card__subtitle">511 products</p>
            </div>   
        </a>
        <span>
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="12" height="12" x="0" y="0" viewBox="0 0 480.026 480.026" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path d="m475.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312-134.368 149.28c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/><path d="m267.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312L4.114 389.293c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/></g></svg>
        </span>
    `;

    const category4 = ` 
    <a href="sub-product-catalog.html" class="category__wrapper">
        <img class="category__image" src="img/catalog/product/item-4.png" alt="item-4">
        <div>
            <h5 class="category__title">Rubin</h5>
            <p class="card__subtitle">773 products</p>
        </div>
    </a>
    <span>
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="12" height="12" x="0" y="0" viewBox="0 0 480.026 480.026" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path d="m475.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312-134.368 149.28c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/><path d="m267.922 229.325-144-160c-3.072-3.392-7.36-5.312-11.904-5.312h-96a16.052 16.052 0 0 0-14.624 9.472c-2.56 5.792-1.504 12.544 2.72 17.216l134.368 149.312L4.114 389.293c-4.224 4.704-5.312 11.456-2.72 17.216 2.592 5.792 8.32 9.504 14.624 9.504h96c4.544 0 8.832-1.952 11.904-5.28l144-160c5.472-6.08 5.472-15.36 0-21.408z" fill="#725597" data-original="#000000" class="" opacity="1"/></g></svg>
    </span>
    `;

    const category5 = ` 
    <a href="sub-product-catalog.html" class="category__wrapper">
        <img class="category__image" src="img/catalog/product/item-5.png" alt="item-5">
        <div>
            <h5 class="category__title">Fionite</h5>
            <p class="card__subtitle">206 products</p>
        </div>       
    </a>
    `;

    const categories = [

    {
        code: 'category1',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category-2',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category-3',
        url: 'index.html',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category-4',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category-5',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category-6',
        name: category1,
        parent: null,
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category1-1',
        name: category2,
        parent: 'category1',
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category1-2',
        name: category2,
        parent: 'category1',
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category1-3',
        name: category2,
        parent: 'category1',
        url: 'sub-product-catalog.html',
    },
    {
        code: 'category1-4',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-4',
    },
    {
        code: 'category1-5',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-5',
    },
    {
        code: 'category1-6',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-6',
    },
    {
        code: 'category1-7',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-7',
    },
    {
        code: 'category1-8',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-8',
    },
    {
        code: 'category1-9',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-9',
    },
    {
        code: 'category1-10',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-10',
    },
    {
        code: 'category1-11',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-11',
    },
    {
        code: 'category1-12',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-12',
    },
    {
        code: 'category1-13',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-13',
    },

    {
        code: 'category1-14',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-14',
    },

    {
        code: 'category1-15',
        name: category2,
        parent: 'category1',
        url: 'index.html?category=category1-15',
    },
    {
        code: 'category-2-1',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-1',
    },
    {
        code: 'category-2-2',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-2',
    },
    {
        code: 'category-2-3',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-3',
    },
    {
        code: 'category-2-4',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-4',
    },
    {
        code: 'category-2-5',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-5',
    },
    {
        code: 'category-2-6',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-6',
    },
    {
        code: 'category-2-7',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-7',
    },
    {
        code: 'category-2-8',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-8',
    },
    {
        code: 'category-2-9',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-9',
    },
    {
        code: 'category-2-10',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-10',
    },
    {
        code: 'category-2-11',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-11',
    },
    {
        code: 'category-2-12',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-12',
    },
    {
        code: 'category-2-13',
        name: category3,
        parent: 'category-2',
        url: 'index.html?category=category-2-13',
    },
    {
        code: 'category-3-1',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-1',
    },
    {
        code: 'category-3-2',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-2',
    },
    {
        code: 'category-3-3',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-3',
    },
    {
        code: 'category-3-4',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-4',
    },
    {
        code: 'category-3-5',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-5',
    },
    {
        code: 'category-3-6',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-6',
    },
    {
        code: 'category-3-7',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-7',
    },
    {
        code: 'category-3-8',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-8',
    },
    {
        code: 'category-3-9',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-9',
    },
    {
        code: 'category-3-10',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-10',
    },
    {
        code: 'category-3-11',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-11',
    },
    {
        code: 'category-3-12',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-12',
    },
    {
        code: 'category-3-13',
        name: category1,
        parent: 'category-3',
        url: 'index.html?category=category-3-13',
    },
    {
        code: 'category-4-1',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-1',
    },
    {
        code: 'category-4-2',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-2',
    },
    {
        code: 'category-4-3',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-3',
    },
    {
        code: 'category-4-4',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-4',
    },
    {
        code: 'category-4-5',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-5',
    },
    {
        code: 'category-4-6',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-6',
    },
    {
        code: 'category-4-7',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-7',
    },
    {
        code: 'category-4-8',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-8',
    },
    {
        code: 'category-4-9',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-9',
    },
    {
        code: 'category-4-10',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-10',
    },
    {
        code: 'category-4-11',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-11',
    },
    {
        code: 'category-4-12',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-12',
    },
    {
        code: 'category-4-13',
        name: category1,
        parent: 'category-4',
        url: 'index.html?category=category-4-13',
    },
    {
        code: 'category-5-1',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-1',
    },
    {
        code: 'category-5-2',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-2',
    },
    {
        code: 'category-5-3',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-3',
    },
    {
        code: 'category-5-4',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-4',
    },
    {
        code: 'category-5-5',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-5',
    },
    {
        code: 'category-5-6',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-6',
    },
    {
        code: 'category-5-7',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-7',
    },
    {
        code: 'category-5-8',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-8',
    },
    {
        code: 'category-5-9',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-9',
    },
    {
        code: 'category-5-10',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-10',
    },
    {
        code: 'category-5-11',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-11',
    },
    {
        code: 'category-5-12',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-12',
    },
    {
        code: 'category-5-13',
        name: category1,
        parent: 'category-5',
        url: 'index.html?category=category-5-13',
    },
    {
        code: 'category-6-1',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-1',
    },
    {
        code: 'category-6-2',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-2',
    },
    {
        code: 'category-6-3',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-3',
    },
    {
        code: 'category-6-4',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-4',
    },
    {
        code: 'category-6-5',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-5',
    },
    {
        code: 'category-6-6',
        name: category1,
        parent: 'category-6',
        url: 'index.html?category=category-6-6',
    },
    {
        code: 'category1-1-1',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-1',
    },
    {
        code: 'category1-1-2',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-2',
    },
    {
        code: 'category1-1-3',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-3',
    },
    {
        code: 'category1-1-4',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-4',
    },
    {
        code: 'category1-1-5',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-5',
    },
    {
        code: 'category1-1-6',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-6',
    },
    {
        code: 'category1-1-7',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-7',
    },
     {
        code: 'category1-1-8',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-8',
    },
    {
        code: 'category1-1-9',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-9',
    },
    {
        code: 'category1-1-6',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-6',
    },
    {
        code: 'category1-1-10',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-10',
    },
    {
        code: 'category1-1-11',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-11',
    },
    {
        code: 'category1-1-12',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-12',
    },
    {
        code: 'category1-1-13',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-13',
    },
    {
        code: 'category1-1-14',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-14',
    },
    {
        code: 'category1-1-15',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-15',
    },
    {
        code: 'category1-1-16',
        name: category3,
        parent: 'category1-1',
        url: 'index.html?category=category1-1-16',
    },
    {
        code: 'category1-2-1',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-1',
    },
    {
        code: 'category1-2-2',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-2',
    },
    {
        code: 'category1-2-3',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-3',
    },
    {
        code: 'category1-2-4',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-4',
    },
     {
        code: 'category1-2-5',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-5',
    },
    {
        code: 'category1-2-6',
        name: category1,
        parent: 'category1-2',
        url: 'index.html?category=category1-2-6',
    },
    {
        code: 'category1-3-1',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-1',
    },
    {
        code: 'category1-3-2',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-2',
    },
    {
        code: 'category1-3-3',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-3',
    },
    {
        code: 'category1-3-4',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-4',
    },
    {
        code: 'category1-3-5',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-5',
    },
    {
        code: 'category1-3-6',
        name: category1,
        parent: 'category1-3',
        url: 'index.html?category=category1-3-6',
    },
    {
        code: 'category1-4-1',
        name: category3,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-1',
    },
    {
        code: 'category1-4-2',
        name: category1,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-2',
    },
    {
        code: 'category1-4-3',
        name: category1,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-3',
    },
    {
        code: 'category1-4-4',
        name: category1,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-4',
    },
    {
        code: 'category1-4-5',
        name: category1,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-5',
    },
    {
        code: 'category1-4-6',
        name: category1,
        parent: 'category1-4',
        url: 'index.html?category=category1-4-6',
    },
    {
        code: 'category1-5-1',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-1',
    },
    {
        code: 'category1-5-2',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-2',
    },
    {
        code: 'category1-5-3',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-3',
    },
    {
        code: 'category1-5-4',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-4',
    },
    {
        code: 'category1-5-5',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-5',
    },
    {
        code: 'category1-5-6',
        name: category1,
        parent: 'category1-5',
        url: 'index.html?category=category1-5-6',
    },
    {
        code: 'category1-6-1',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-1',
    },
    {
        code: 'category1-6-2',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-2',
    },
    {
        code: 'category1-6-3',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-3',
    },
    {
        code: 'category1-6-4',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-4',
    },
    {
        code: 'category1-6-5',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-5',
    },
    {
        code: 'category1-6-6',
        name: category1,
        parent: 'category1-6',
        url: 'index.html?category=category1-6-6',
    },
    {
        code: 'category-2-2-2',
        name: category1,
        parent: 'category-2-2',
        url: 'index.html?category=category-2-2-2',
    },
    {
        code: 'category-3-3-3',
        name: category1,
        parent: 'category-3-3',
        url: 'index.html?category=category-3-3-3',
    },
    {
        code: 'category-4-4-4',
        name: category3,
        parent: 'category-4-4',
        url: 'index.html?category=category-4-4-4',
    },
    {
        code: 'category1-1-1-1',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-1',
    },
    {
        code: 'category1-1-1-2',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-2',
    },
    {
        code: 'category1-1-1-3',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-3',
    },
    {
        code: 'category1-1-1-4',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-4',
    },
    {
        code: 'category1-1-1-5',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-5',
    },
    {
        code: 'category1-1-1-6',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-6',
    },
    {
        code: 'category1-1-1-7',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-7',
    },
    {
        code: 'category1-1-1-8',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-8',
    },
    {
        code: 'category1-1-1-9',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-9',
    },
    {
        code: 'category1-1-1-10',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-10',
    },
    {
        code: 'category1-1-1-11',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-11',
    },
    {
        code: 'category1-1-1-12',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-12',
    },
    {
        code: 'category1-1-1-13',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-13',
    },
    {
        code: 'category1-1-1-14',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-14',
    },
    {
        code: 'category1-1-1-15',
        name: category4,
        parent: 'category1-1-1',
        url: 'index.html?category=category1-1-1-15',
    },
    {
        code: 'category-2-2-2-2',
        name: category1,
        parent: 'category-2-2-2',
        url: 'index.html?category=category-2-2-2-2',
    },
    {
        code: 'category-3-3-3-3',
        name: category4,
        parent: 'category-3-3-3',
        url: 'index.html?category=category-3-3-3-3',
    },
    {
        code: 'category-1-1-1-1-1',
        name: category5,
        parent: 'category-1-1-1-1',
        url: 'index.html?category=category-1-1-1-1-1',
    },
    {
        code: 'category1-1-1-1-2',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-2',
    },
    {
        code: 'category1-1-1-1-3',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-3',
    },
    {
        code: 'category1-1-1-1-4',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-4',
    },
    {
        code: 'category1-1-1-1-5',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-5',
    },
    {
        code: 'category1-1-1-1-6',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-6',
    },
    {
        code: 'category1-1-1-1-7',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-7',
    },
    
    {
        code: 'category1-1-1-1-8',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-8',
    },

    {
        code: 'category1-1-1-1-9',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-9',
    },

    {
        code: 'category1-1-1-1-10',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-10',
    },

    {
        code: 'category1-1-1-1-11',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-11',
    },

    {
        code: 'category1-1-1-1-12',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-12',
    },

    {
        code: 'category1-1-1-1-13',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-13',
    },

    {
        code: 'category1-1-1-1-14',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-14',
    },

    {
        code: 'category1-1-1-1-15',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-15',
    },

    {
        code: 'category1-1-1-1-16',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-16',
    },

    {
        code: 'category1-1-1-1-17',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-17',
    },

    {
        code: 'category1-1-1-1-18',
        name: category5,
        parent: 'category1-1-1-1',
        url: 'index.html?category=category1-1-1-1-18',
    },
];

function createColumn(categories, counter) {
    const wrapper = document.querySelector('.categories')
    // даем Date атрибут counter //
    const column = document.createElement("li")
    column.classList.add("category__list")
    column.classList.add(`column-${counter}`)

    for (let category of categories) { 
        column.appendChild(categoryItem(category))
    }

    wrapper.append(column)
    return column
}

function categoryItem(category) {
    const categoryItem = document.createElement("a")
    categoryItem.classList.add("category__item");
    categoryItem.dataset.code = category.code
    categoryItem.dataset.url = category.url
    categoryItem.innerHTML = category.name
    
    categoryItem.addEventListener('click', (e) => { 
        let url = ''
        if (!e.target.classList.contains('category__item')) {
            url = e.target.closest('.category__item').dataset.url
        } else {
            url = e.target.dataset.url
        }
        window.location.href = url
    })

    categoryItem.addEventListener('mouseover', (e) => {
        let code = ''
        if (!e.target.classList.contains('category__item')) {
            code = e.target.closest('.category__item').dataset.code
        } else {
            code = e.target.dataset.code
        }
        // заменнить Date атрибут при скрытии последней колонки // 
        removeColumns(code)

        if (columntExists(code)) {
            return
        }
        const items = categories.filter(category => category.parent === code)
        const columnsSize = document.querySelectorAll('.category__list').length
        const column = createColumn(items, columnsSize + 1)
        column.dataset.parent = code
        const wrapper = document.querySelector('.categories')

        
        if (columnsSize >= 1) {
            wrapper.classList.add('categories-view-column-1');
        } else {
            wrapper.classList.remove('categories-view-column-1');
        }

        if (columnsSize >= 2) {
            wrapper.classList.add('categories-view-column-2');
        } else {
            wrapper.classList.remove('categories-view-column-2');
        }

        if (columnsSize >= 3) {
            wrapper.classList.add('categories-view-column-3');
        }  else {
            wrapper.classList.remove('categories-view-column-3');
        }

        if (columnsSize >= 4) {
            wrapper.classList.add('categories-view-column-4');
        }  else {
            wrapper.classList.remove('categories-view-column-4');
        }

        if (columnsSize >= 5) {
            wrapper.classList.add('categories-view-column-5');
        }  else {
            wrapper.classList.remove('categories-view-column-5');
        }
    })
    
    return categoryItem
}

function removeColumns(code, columns = []) {
    const category = categories.find(c => c.code === code)
    
    if (category.parent !== null) {
        columns.push(category.parent)
        removeColumns(category.parent, columns)
    } else {
        if (!columns.length) { // select only parent id
            getColumns().forEach(e => e.remove());
        } else {
            for (let col of getColumns()) {
                if (!columns.includes(col.dataset.parent)) {
                    col.remove()
                }
            }
        }
    }

    
}

function getColumns() {
    return document.querySelectorAll('.category__list[data-parent]')   
}

function columntExists(code) {
    const columns = getColumns();
    for (let column of columns) {
        if (column.dataset.parent == code) {
            return true
        }
    }

    return false
}

    createColumn(categories.filter(category => category.parent === null), 1);
    
// categories__modal

const categoriesButton = document.querySelector('.categories__button');
const categoriesModal = document.querySelector('.categories__modal');

categoriesButton.addEventListener('click', () => {  
    categoriesModal.classList.add('active');
    overlayActive();
    asideMain.classList.add('index');
})


// modal categories //

const categories1 = document.querySelector('.categories-1');
const categories2 = document.querySelector('.categories-2');
const categories3 = document.querySelector('.categories-3');

const categoriesList = document.querySelectorAll('.categories-mobile__wrapper');

categoriesList.forEach((categoryList) => {
    const category1 = categoryList.querySelectorAll('.category-1');
    const category2 = categoryList.querySelectorAll('.category-2');
   
    category1.forEach((cat1) => {   
        cat1.addEventListener('click', () => {
            categories1.style.display = "none";
            categories2.style.display = "block"
        })

        const backButton = categories2.querySelector('.back__button');
        backButton.addEventListener('click', () => {
            categories1.style.display = "block";
            categories2.style.display = "none";
        })
    })

    category2.forEach((cat2) => {   
        cat2.addEventListener('click', () => {
            categories2.style.display = "none";
            categories3.style.display = "block"
        })

        const backButton = categories3.querySelector('.back__button');
        backButton.addEventListener('click', () => {
            categories2.style.display = "block";
            categories3.style.display = "none";
        })
    })
})
}




