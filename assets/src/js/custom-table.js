if (document.getElementsByClassName('custom-scroll__table').length > 0) {
    const costumeTableMain = document.querySelector('.custom-scroll__table');
    const columnLast = document.querySelectorAll('.cell-sticky');
    const columnFirst = document.querySelectorAll('.cell__link');
    const rowHeader = document.querySelector('.custom-scroll-table__header');
    const lastColName = document.querySelector('.last-col-name');
    const rows = document.querySelectorAll('.row')

    rows.forEach((row) => {
        row.style.width = "2520px";
    })

    rowHeader.style.width = "2520px";

    costumeTableMain.addEventListener('scroll', function() {
        
        if (costumeTableMain.offsetWidth + costumeTableMain.scrollLeft >= costumeTableMain.scrollWidth) {
            columnLast.forEach((col) => {
                col.classList.remove('boxShadow');
            })

            columnFirst.forEach((col2) => {
                col2.classList.add('boxShadow');
            })

            lastColName.classList.remove('boxShadow');

            rowHeader.classList.add('boxShadow');

        } else {
            columnLast.forEach((col) => {
                col.classList.add('boxShadow');
            })

            columnFirst.forEach((col2) => {
                col2.classList.remove('boxShadow');
            })

            lastColName.classList.add('boxShadow');

            rowHeader.classList.remove('boxShadow');
        }

        if (costumeTableMain.offsetWidth + costumeTableMain.scrollLeft < costumeTableMain.scrollWidth) {
            columnFirst.forEach((col2) => {
                col2.classList.add('boxShadow');
            })

            rowHeader.classList.add('boxShadow');
        }

        if (this.scrollLeft == 0) {
            columnFirst.forEach((col2) => {
                col2.classList.remove('boxShadow');
            })

            rowHeader.classList.remove('boxShadow');
        }

        
    })

    reduceMenu.addEventListener('click', () => {
        costumeTableMain.classList.toggle('max-width');
    })
}





