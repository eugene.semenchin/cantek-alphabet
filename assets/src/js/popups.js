const popups1 = document.querySelector('.popup__switch');
const popups2 = document.querySelector('.popup__delete');
const popups3 = document.querySelector('.popup__done');
const popupsButtons1 = document.querySelectorAll('.popup-switch__button');
const popupsButtons2 = document.querySelectorAll('.popup-delete__button');
const popupsButtons3 = document.querySelectorAll('.popup-done__button');

popupsButtons1.forEach((btn1) => {
    btn1.addEventListener('click', () => {
        popups1.classList.toggle('active');
        overlayActive();
    })
});

popupsButtons2.forEach((btn2) => {
    btn2.addEventListener('click', () => {
        popups2.classList.toggle('active');
        overlayActive();
    })

    const confirmationButton = popups2.querySelector('.confirmation__button');

    confirmationButton.addEventListener('click', () => {
        popups2.classList.add('confirmation'); 
    })
});

popupsButtons3.forEach((btn3) => {
    btn3.addEventListener('click', () => {
        popups3.classList.toggle('active');
        overlayActive();
    })
});

