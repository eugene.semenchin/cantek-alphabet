const orderContent = document.querySelector('.basket__block');
const orderButton = document.querySelector('.draw__button');
const generateBlock = document.querySelector('.Querying__block');

if(window.location.toString().indexOf('order-create.html')>0) {
    const orderFinal = () => {
        orderContent.style.display = 'none';
        generateBlock.style.display = 'flex';
    }
    
    orderButton.addEventListener('click', () => {
        orderFinal();
    })
}

if(window.location.toString().indexOf('order-request.html')>0) {
    const orderFinal = () => {
        orderContent.style.display = 'none';
        generateBlock.style.display = 'flex';
    }
    
    orderButton.addEventListener('click', () => {
        orderFinal();
    })
}

