// datalist //

const searchInput = document.querySelector('.search__input');
const searchDatalist = document.querySelector('.search__datalist');
const searchOption = document.querySelectorAll('.option__item .select__item');
const searchClose = document.querySelector('.close__search');
const searchLoupe = document.querySelector('.loupe__search');

if (document.getElementsByClassName('search__input').length > 0) {

  searchInput.onfocus = function () {
    // searchDatalist.style.display = 'block';
    searchDatalist.classList.toggle('active');
    searchInput.style.borderRadius = "8px 8px 0 0";
    searchClose.classList.toggle('active');
    searchLoupe.classList.toggle('hidden');
};

searchClose.addEventListener('click', () => {
  searchDatalist.classList.remove('active');
  searchClose.classList.remove('active');
  searchLoupe.classList.remove('hidden');
  searchInput.style.borderRadius = "8px 8px 8px 8px";
})


for (let option of searchDatalist.options) {
    option.onclick = function () {
    searchInput.value = option.value;
    // searchDatalist.style.display = 'none';
    searchInput.style.borderRadius = "5px";
    }
};

searchInput.oninput = function() {
    currentFocus = -1;
    var text = searchInput.value.toUpperCase();
    for (let option of searchDatalist.options) {
      if(option.value.toUpperCase().indexOf(text) > -1){
        option.style.display = "block";
        
    }else{
      option.style.display = "none";
      }
    };
}
  
var currentFocus = -1;
  searchInput.onkeydown = function(e) {
    if(e.keyCode == 40){
      currentFocus++
     addActive(searchDatalist.options);
    }
    else if(e.keyCode == 38){
      currentFocus--
     addActive(searchDatalist.options);
    }
    else if(e.keyCode == 13){
      e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (searchDatalist.options) searchDatalist.options[currentFocus].click();
          }
    }
  }
  
  function addActive(x) {
      if (!x) return false;
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      x[currentFocus].classList.add("active");
    }
    function removeActive(x) {
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("active");
      }
    }
}


