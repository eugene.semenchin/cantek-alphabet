const listBlock = document.querySelectorAll('.list__block');

listBlock.forEach((block) => {
    var list = block.querySelector('.hidden__list');
    var showButton = block.querySelector('.show__button');
    var text = block.querySelector('.text');
    showButton.addEventListener('click', () => {
        list.classList.toggle('show');
        showButton.classList.toggle('rotate');
        text.innerHTML = (text.innerHTML === 'Collapse') ? text.innerHTML = 'Expand' : text.innerHTML = 'Collapse';
    })
})
