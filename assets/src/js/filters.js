const filterButton = document.querySelector('.filter__button');
const filtersModal = document.querySelector('.filter__modal');
const filtersClose = document.querySelector('.filters-modal__close');


if (document.getElementsByClassName('filter__modal').length > 0) {
    const filterModalActive = function () {
        filtersModal.classList.toggle('active');
    }
    
    const filtersModalClose = function () {
        filtersModal.classList.remove('active');
    }
    
    filterButton.addEventListener("click", function () {
        filterModalActive();
        overlayActive();
    });
    
    filtersClose.addEventListener('click', function () {
        filtersModalClose();
        overlayHide();
    })
}



