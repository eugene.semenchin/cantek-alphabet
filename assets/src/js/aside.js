// Aside reduce //
const reduceMenu = document.querySelector('.reduce__menu');
const asideMain = document.querySelector('.aside__main');
const burgerButton = document.querySelector('.burger-menu__button');
const closeMenu = document.querySelector('.close__menu');
const asideLogo = document.querySelector('.no-aside-logo');

if (document.getElementsByClassName('reduce__menu').length > 0) {
    const asideReduce = function() {
        asideMain.classList.toggle('aside__reduce');
    }
    
    const activeMenu = function () {
        asideMain.classList.toggle('aside__visible');   
    }
    
    reduceMenu.addEventListener('click', function () {
        asideReduce();
        reduceMenu.classList.toggle('reduce__rotate');
        
        // transitionHide();
    });
    
    burgerButton.addEventListener('click', function () {
        activeMenu();
        overlayActive();
    })
    
    closeMenu.addEventListener('click', function () {
        activeMenu();
        overlayHide();
    })
}


if(window.location.toString().indexOf('order-create.html')>0) {
    asideMain.style.display = 'none';
    burgerButton.style.display = 'none';
    asideLogo.style.display = 'flex';
}

if(window.location.toString().indexOf('order-request.html')>0) {
    asideMain.style.display = 'none';
    burgerButton.style.display = 'none';
    asideLogo.style.display = 'flex';
}
