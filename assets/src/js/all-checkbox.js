if (document.getElementsByClassName('all-checkbox').length > 0) {
    
    document.querySelector(".all-checkbox").addEventListener('click', function(e) {

    [].forEach.call(document.querySelectorAll('.checkout__input'), c => c.checked = this.checked);
        
    }); 

    const downloadSelected = document.querySelector('.download-selected');
    const checkbox = document.querySelectorAll('.checkout__input')
    
    checkbox.forEach((check) => {
        check.addEventListener('change', () => {
            if (
                document.querySelectorAll('.checkout__input:checked').length >= 2
            ){
                downloadSelected.classList.add('active');
            } else {
                downloadSelected.classList.remove('active');
            }
             
        })
    })
}

   