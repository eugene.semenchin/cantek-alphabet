const profileButtons = document.querySelectorAll('.profile__button');
const profileModal = document.querySelector('.profile__modal');
const profileClose = document.querySelector('.profile-modal__close');

if (document.getElementsByClassName('profile__button').length > 0) {
    const profileModalActive = function () {
        profileModal.classList.toggle('active');
    }
    
    const profileModalClose = function () {
        profileModal.classList.remove('active');
    }
    
    profileButtons.forEach(function (i) {
        i.addEventListener("click", function () {
            profileModalActive();
            overlayActive();
        });
    });
    
    profileClose.addEventListener('click', function () {
        profileModalClose();
        overlayHide();
    })
}


