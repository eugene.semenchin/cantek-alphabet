const resumeDialogueButton = document.querySelector('.resume__dialogue')
const dialogueChange = document.querySelector('.dialogue__change');
const dialogueButton = document.querySelectorAll('.dialogue__button');

if (document.getElementsByClassName('appeal__block').length > 0) {
    resumeDialogueButton.addEventListener('click', function() {
        dialogueChange.style.display = 'flex';
        resumeDialogueButton.style.display = 'none';
    });
    
    
    dialogueButton.forEach((button) => {
        button.addEventListener('click', function() {
            dialogueChange.style.display = 'none';
            resumeDialogueButton.style.display = 'flex';
        })
    });
}

