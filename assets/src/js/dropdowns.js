const dropdownBlocks = document.querySelectorAll('.dropdown__block');
const dropdownButton = document.querySelector('.dropdown__button');
const dropdownContent = document.querySelector('.dropdown__content');
const dropupButton = document.querySelector('.personal-manager__button');
const asideHeight = document.querySelector('.aside-height');
const backgroundLines = document.querySelectorAll('.background-line');

if (document.getElementsByClassName('dropdown__button').length > 0) {
    dropdownBlocks.forEach((dropdownBlock) => {
        var dropdownButton = dropdownBlock.querySelector('.dropdown__button');
        var dropdownContent = dropdownBlock.querySelector('.dropdown__content');
        var dropdownArrow = dropdownBlock.querySelector('.arrow__button');
       
        dropdownButton.addEventListener('click', function () {
            dropdownContent.classList.toggle('active');
           
            if (dropdownButton.classList.contains('properties-show')) {
                dropdownButton.innerHTML = (dropdownButton.innerHTML === 'Collapse properties') ? dropdownButton.innerHTML = 'Show properties' : dropdownButton.innerHTML = 'Collapse properties';
            }

            if (dropdownArrow.classList.contains('arrow-contains')) {
                dropdownArrow.classList.toggle('active');
            } 
        })
    })
}

if (document.getElementsByClassName('aside__links').length > 0) {
    // aside dropdown //
    // const asideMain = document.querySelector('.aside__main');
    const asidelinksButton = document.querySelector('.aside__links');

    asidelinksButton.addEventListener('click', () => {
        asidelinksButton.classList.toggle('active');
    })

        if (asideMain.classList.contains('aside__reduce')) {
            asidelinksButton.addEventListener('click', () => {
                asidelinksButton.classList.remove('active');
            })
    }
}

if (document.getElementsByClassName('custom-scroll__table').length > 0) {
    const customTable = document.querySelector(".table--more");
    const buttonTableMore = document.querySelector(".show--more");

    buttonTableMore.addEventListener('click', () => {
        customTable.classList.toggle('active');
        buttonTableMore.classList.toggle('hidden');
    })
}

if (document.getElementsByClassName('aside__main-navigation').length > 0) {
    dropupButton.addEventListener('click', () => {
        asideHeight.classList.toggle('active');
    })

}

if (document.getElementsByClassName('background-line').length > 0) {
    backgroundLines.forEach((lines) => {
        var backgroundButton = lines.querySelector('.background__button');
       
        backgroundButton.addEventListener('click', function () {
            lines.classList.toggle('active');
        })
    })
}


