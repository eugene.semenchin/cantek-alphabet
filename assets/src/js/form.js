const editFieldsButton = document.querySelector(".edit-fields__button");
const fields = document.querySelectorAll(".form__wrapper");
const saveChanges = document.querySelector('.save-changes');
const profilePhoto = document.querySelector('.profile__photo');
const repeatPassword = document.querySelector('.repeat-password');
const editingButton = document.querySelector('.editing__button');
const applyButton = document.querySelector('.apply__button');
const detailsSelect = document.querySelector('.details-select');
const profileEditDel = document.querySelectorAll('.profile__edit-del');

if (document.getElementsByClassName('edit-fields__button').length > 0) {
    editFieldsButton.addEventListener('click', () => {
        fields.forEach((field) => {
            const inputField = field.querySelector('.form__input')
            field.classList.remove('disable');
            inputField.removeAttribute("readonly");
            saveChanges.classList.add('active');
            editFieldsButton.classList.add('hide');
        })

        profilePhoto.classList.add('add');
        repeatPassword.classList.add('show');
    })
}

if (document.getElementsByClassName('editing__button').length > 0) {
    editingButton.addEventListener('click', () => {
        fields.forEach((field) => {
            field.classList.remove('disable');
        })

        editingButton.classList.add('hidden');
        applyButton.classList.add('active');
        detailsSelect.classList.add('edit');

        profileEditDel.forEach((editdel) => {
            editdel.classList.add('edit');
        })
    })

    applyButton.addEventListener('click', () => {
        fields.forEach((field) => {
            field.classList.add('disable');
        })

        editingButton.classList.remove('hidden');
        applyButton.classList.remove('active');
        detailsSelect.classList.remove('edit');

        profileEditDel.forEach((editdel) => {
            editdel.classList.remove('edit');
        })
    })
}


