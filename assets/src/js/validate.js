if (document.getElementsByClassName('sum-filter').length > 0) {
    const sumFilter = document.querySelectorAll('.sum-filter');
    
    sumFilter.forEach((sumfil) => {
        const sumInput = sumfil.querySelector('.sum__input');
        const closeSum = sumfil.querySelector('.close-sum');

        sumInput.oninput = () => {
            sumfil.classList.add('active');
        }

        closeSum.addEventListener('click', () => {
            sumInput.value = '';
            sumfil.classList.remove('active');
        })
        
    })
}

// if (document.getElementsByClassName('main__form').length > 0) {
//     const mainForm = document.querySelectorAll('.main__form');

//     mainForm.forEach((form) => {
        
//         function showError(input) {
//             const formControl = input.parentElement;
//             formControl.className = 'form__wrapper error';
//         }

//         function showSucces(input) {
//             const formControl = input.parentElement;
//             formControl.className = 'form__wrapper success';
//         }

//         function checkRequired(inputArr) {
//             inputArr.forEach(function(input){
//                 if(input.value.trim() === ''){
//                     showError(input,`${getFieldName(input)} is required`)
//                 }else {
//                     showSucces(input);
//                 }
//             });
//         }

//         function checkLength(input, min) {
//             if(input.value.length < min) {
//                 showError(input, `${getFieldName(input)} must be at least ${min} characters`);
//             } else {
//                 showSucces(input);
//             }
//         }

//         function getFieldName(input) {
//             return input.className.charAt(0).toUpperCase() + input.className.slice(1);
//         }
        
//         const emailWrapper = form.querySelectorAll('.form__wrapper');

//         emailWrapper.forEach((wrap) => {
//             const showPassword = wrap.querySelector('.show__password');
//             const passwordInput = wrap.querySelector('.password__input');

//         if(showPassword) {
//             showPassword.addEventListener('click', () => {
//                 passwordInput.setAttribute('type', 'text');
//                 showPassword.classList.add('show');
//             })
//         } 
//         })

//         form.addEventListener('submit',function(e) {
//             e.preventDefault();
            
//             const symbols = form.querySelectorAll('.symbols');

//             symbols.forEach((sym) => {
//                 checkRequired([sym]);
//                 checkLength(sym,3);
//             })

//             // checkRequired([username]);
//             // checkLength(username,3,15);
//         });
//     })
// }

if (document.getElementsByClassName('authorization__form').length > 0) {
    const authorizationForm = document.querySelector('.authorization__form');
    const formInputs = authorizationForm.querySelectorAll('.form__input');
    const loginButton = authorizationForm.querySelector('.login__button');
    const emailWrapper = authorizationForm.querySelector('.email__wrapper');
    const passwordWrapper = authorizationForm.querySelector('.password__wrapper');
    const showPassword = authorizationForm.querySelector('.show__password')
    const passwordInput = authorizationForm.querySelector('.password__input');
    const emailInput = document.querySelector('.email__input');
    const validFormArr = [];

    
    if (emailInput.hasAttribute("data-reg")) {
        emailInput.setAttribute("is-valid", "false");
        validFormArr.push(emailInput);
        emailInput.addEventListener("input", inputHandler)
    }

    if (passwordInput.hasAttribute("data-reg")) {
        passwordInput.setAttribute("is-valid", "false");
        validFormArr.push(passwordInput);
        passwordInput.addEventListener("input", passwordHandler)
    }
   

    function inputHandler({ target }) {
        const inputValue = target.value;
        const inputReg = target.getAttribute("data-reg");
        const reg = new RegExp(inputReg);
        if (reg.test(inputValue)) {
            target.setAttribute("is-valid", "true");
            emailWrapper.classList.add('success')
        } else {
            target.setAttribute("is-valid", "false");
            emailWrapper.classList.add('error')
        }
    }

    function passwordHandler({ target }) {
        const inputValue = target.value;
        const inputReg = target.getAttribute("data-reg");
        const reg = new RegExp(inputReg);
        if (reg.test(inputValue)) {
            target.setAttribute("is-valid", "true");
            passwordWrapper.classList.add('success')
        } else {
            target.setAttribute("is-valid", "false");
            passwordWrapper.classList.add('error')
        }
    }

    showPassword.addEventListener('click', ()=> {
        showPassword.classList.add('show');
        passwordInput.type="text"; 
    })

    loginButton.addEventListener('click', () => {
        const inputsLength = validFormArr.length;
        const inputsOnlyValid = validFormArr.filter((input) => input.getAttribute('is-valid') === "true");

        if (inputsLength !== inputsOnlyValid.length) {
            emailWrapper.classList.add('error')
            passwordWrapper.classList.add('error')
        } else {

            location.href = 'profile-select.html';
          
        }

    })
   
}






