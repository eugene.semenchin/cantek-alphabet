const mainModal = document.querySelectorAll('.modal-window');

mainModal.forEach((main) => {
    const closeModal = main.querySelectorAll('.modal__hidden');

    // closeModal.addEventListener('click', () => {
    //     main.classList.remove('active');
    //     overlayHide();
    //     asideMain.classList.remove('index');
    // })

    closeModal.forEach((close) => {
        close.addEventListener('click', () => {
            main.classList.remove('active');
            overlayHide();
            asideMain.classList.remove('index');
        })
    })
})


const orderSearch = document.querySelector('.order-search__modal');
const documentSearchButton = document.querySelector('.document-search');


if (document.getElementsByClassName('document-search').length > 0) {
    documentSearchButton.addEventListener('click', () => {
        orderSearch.classList.toggle('active');
    })
}

if (document.getElementsByClassName('configure__button').length > 0) {
    const configureModal = document.querySelector('.configure__modal');
    const configureButton = document.querySelector('.configure__button');

    configureButton.addEventListener('click', () => {
        configureModal.classList.toggle('active');
        overlayActive();
    })
}

if (document.getElementsByClassName('sub-modal__button').length > 0) {
    const subModal = document.querySelector('.sub-modal');
    const subModalButton = document.querySelectorAll('.sub-modal__button');

    subModalButton.forEach((subbutton) => {
        subbutton.addEventListener('click', () => {
            subModal.classList.toggle('active');
            overlayActive();
        })
    })
}

if (document.getElementsByClassName('sort-mobile').length > 0) {
    const sortModal = document.querySelector('.sort-mobile__modal');
    const sortMobileButton = document.querySelectorAll('.sort-mobile');

    sortMobileButton.forEach((sortbutton) => {
        sortbutton.addEventListener('click', () => {
            sortModal.classList.toggle('active');
            overlayActive();
        })
    })
}

if (document.getElementsByClassName('delivery-conditions___modal').length > 0) {
    const deliveryConditionsModal = document.querySelector('.delivery-conditions___modal');
    const deliveryButton = document.querySelector('.delivery__button');

    deliveryButton.addEventListener('click', () => {
        deliveryConditionsModal.classList.toggle('active');
        overlayActive();
    })
}

if (document.getElementsByClassName('delivery-payment___modal').length > 0) {
    const deliveryPaymentModal = document.querySelector('.delivery-payment___modal');
    const deliveryButton = document.querySelector('.payment__button');

    deliveryButton.addEventListener('click', () => {
        deliveryPaymentModal.classList.toggle('active');
        overlayActive();
    })
}








